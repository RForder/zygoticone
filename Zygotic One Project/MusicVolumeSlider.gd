extends HSlider


export var audio_bus_name := "Music Global"

onready var _bus := AudioServer.get_bus_index(audio_bus_name)


func _ready() -> void:
	value = db2linear(AudioServer.get_bus_volume_db(_bus))

func _on_value_changed(sound: float) -> void:
	AudioServer.set_bus_volume_db(_bus, linear2db(sound))

func _on_MusicVolumeSlider_value_changed(new_value):
	value = new_value;
	_on_value_changed((value));
	pass # Replace with function body.
