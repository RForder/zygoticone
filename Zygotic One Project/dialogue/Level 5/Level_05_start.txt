[] [DIANE]

# Trigger at level start, end of dialogue triggers level start

[DIANE , Neutral]

You may have noticed we are not outside. 

$DianeTeleportIn

[,]

Because unfortunately for you, this is where your story ends.

[,]

My goal of destroying this planet was grievously inconvenienced when the organic life forms here shattered my vessel and put up defenses to further stop me. 

[,]

This is why you were created. 

[,]

$DianeSwat

To fight fire with fire, as the saying goes.

[,]

Honestly, I'm surprised you made it this far. The other clones all perished fairly quickly after I sent them out into the world. 

[,]

So, kudos.

$DianeShrug

[,]

You were entertaining while you lasted but all good things must come to an end. I no longer have a use for you, so hand over the collected components and I'll make your ending quick.

[,]

...

[,]

No?

[,]

Then we will do it the hard way.

$DianeShrug

[,]

I will give you one last moment to savor the feeling of aliveness.


[,]

...

[,]

OK.

$DianeGone

[DIANE , Worm]

$DianeWormIn

Time to die.


