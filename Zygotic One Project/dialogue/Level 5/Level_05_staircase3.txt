[] [DIANE]


[DIANE , Worm]

Well, this is awkward.

[,]

Listen, don't worry about that whole monologue right before I chased you down this long hallway with spikes and turrets.

[,]

I was....kidding.

[,]

Simply testing how much you learned from your excursions onto the planet. You did so well, I’m very proud of you. Excellent reaction time and motor skills.

[,]

Now, just hand me the components, and I’ll be happy to send you back out to the planet if you enjoyed it that much.