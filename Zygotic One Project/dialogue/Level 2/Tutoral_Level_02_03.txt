[] [DIANE]

# Upon item pick up at the end of the level. 

[DIANE , Neutral]

Yet another item collected. You're getting pretty good at this.

[DIANE , Frown]

You could manage to be a little faster though. It isn't necessary to explore new areas after you've found an item. There isn't likely anything for you to find.

[DIANE , Neutral]

Just a constructive note.





