[] [DIANE]

#Trigger upon view of the first Anti-Gravity Platform.

[DIANE , Neutral]

Hold on a moment. Did you see what happened with that platform? It was stationary at first. Try using your Time Reversal Device.

[,]

It appears you can use time reversal to move some objects in this environment.

[,]

This may seem contrary to previous lessons, but trust me, it is not. My advice is without question. 

[,]

I told you not to die. That was good advice.

