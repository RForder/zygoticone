

[] [DIANE]

# after levels 1-4

[DIANE, Neutral]

I am so very proud. 

[ , ]

You have collected all the required items and you are still alive. Commendable.

[ , ]

I have one more task for you before we can begin our new wonderful life together.

[ , ]

Take the elevator to the bottom floor and take a right. Do not take a left. There is nothing to see to the left.