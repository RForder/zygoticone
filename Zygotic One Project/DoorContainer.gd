extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var open = false
export var OpenDelay = 0

var touched = false
var time_since_touched = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	if open:
		$PressurePlateDoor/AnimatedSprite.animation = "open"
		$PressurePlateDoor/DoorHitbox/DoorHitboxShape.disabled = true
	else:
		$PressurePlateDoor/AnimatedSprite.animation = "closed"
		$PressurePlateDoor/DoorHitbox/DoorHitboxShape.disabled = false
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	if touched:
		time_since_touched = time_since_touched + delta
	if time_since_touched > OpenDelay:
		touched = false
		time_since_touched = 0
		$PressurePlateDoor/AnimatedSprite.play("opening", not open)
		$PressurePlateDoor/Door_SFX.play()
	return


func _on_AnimatedSprite_animation_finished():
	if $PressurePlateDoor/AnimatedSprite.animation == "opening":
		if not open:
			$PressurePlateDoor/AnimatedSprite.play("closed")
			$PressurePlateDoor/DoorHitbox/DoorHitboxShape.disabled = false
		else:
			$PressurePlateDoor/AnimatedSprite.play("open")
			$PressurePlateDoor/DoorHitbox/DoorHitboxShape.disabled = true
	pass # Replace with function body.


func _on_PressurePlateDoor_body_entered(body):
	print("Detected Body: " + body.name);
	if body.name == "Player":
		time_since_touched = 0
		touched = true
		open = not open
		print("Detected Player")
	pass # Replace with function body.
