extends Node2D

# saves completed levels
var coins_filename = "user://coins_collected.txt";
func saveLevels():
	# open file
	var file = File.new();
	file.open(coins_filename, File.WRITE);
	print("opening new coins_collected.txt");

	# go through each level
	for level in range(collected_coins.size()):
		# save each coin
		for coin in collected_coins[level]:
			var out_str = "";
			out_str += str(level) + " ";
			out_str += coin + "\n";
			file.store_string(out_str);
			pass;
	
	# close file
	file.close();

# loads collected coins
func loadCoinsCollected():
	# empty self
	coin_count = 0;
	collected_coins = [[],[],[],[],[]];
	
	# open file
	var file = File.new();
	if file.file_exists(coins_filename):
		# extract text
		file.open(coins_filename, File.READ);
		var lines = [];
		while not file.eof_reached():
			lines.append(file.get_line());
		
		# show
		for line in lines:
			# split out the items
			print("Load Coin: " + line);
			var items = Array(line.split(' '));
			# check that there are two
			if items.size() == 2:
				var level = int(items[0]);
				var name = items[1];
				collected_coins[level].append(name);
				coin_count += 1;
				print("Adding: " + name);

# adds coin to collected coins list
# takes in the name of the coin as an argument
func collectCoin(coin):
	# print("Collected Coin: " + str(coin));
	# print("Current Level: " + str(current_level));
	collected_coins[current_level-1].append(coin);
	updateCount();
	# print(collected_coins[current_level-1]);
	# print("Coin Count: " + str(coin_count));

# updates the coin count
func updateCount():
	var sum = 0;
	for level in collected_coins:
		sum += level.size();
	coin_count = sum;
	
	# call sceneloader's dialogue box if it's 50
	if coin_count == 50:
		SceneLoader.coinDialogue();

# has this coin already been collected
# use this to remove collected coins at the start of a level
func checkCoin(coin):
	# print("Incoming Check: " + coin);
	# print("My List: " + str(collected_coins[current_level-1]));
	# print("Bool: " + str(collected_coins[current_level-1].has(coin)));
	var flag = collected_coins[current_level-1].has(coin);
	if flag:
		print("Current Level: " + str(current_level));
		print("Removing Coin: " + str(coin));
	return flag;

# dictionary containing coins
var collected_coins = [[],[],[],[],[]];
var current_level = 1; # this is modified by each level's _ready() call
var coin_count = 0;

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");


# Called when the node enters the scene tree for the first time.
func _ready():
	loadCoinsCollected();
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
# func _process(delta):
# 	pass
