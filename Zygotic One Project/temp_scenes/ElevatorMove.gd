extends Node2D


# declare vars
var target_ypos = 0;
var step_size = 2000;
var lerp_spd = 5.0;

# bounds
var max_ypos = 0;
var min_ypos = -8000;


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(event):
	if event is InputEventKey and event.pressed:
		# process keys
		if event.scancode == KEY_T:
			target_ypos += step_size;
		if event.scancode == KEY_G:
			target_ypos -= step_size;
			
		# clamp position
		if target_ypos > max_ypos:
			target_ypos = max_ypos;
		if target_ypos < min_ypos:
			target_ypos = min_ypos;

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# lerp towards position
	position = position.linear_interpolate(Vector2(0, target_ypos), delta * lerp_spd);
