set godot=Godot_v3.2.2-stable_win64.exe
set project_dir=.
git rev-parse --short HEAD > shorthash.tres
%godot% -v --export "Windows Desktop" ..\Export\Windows\zygoticone.exe --path %project_dir%
%godot% -v --export "Linux/X11" ..\Export\Linux\zygoticone.x86_64 --path %project_dir%
%godot% -v --export "Mac OSX" ..\Export\OSX\zygoticone.zip --path %project_dir%