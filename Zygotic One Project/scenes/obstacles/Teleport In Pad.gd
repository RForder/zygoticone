extends KinematicBody2D

# declare name of level to teleport to
export var base_search_path = "res://scenes/Worlds/Crust/";
export var level_name = "Level1.tscn"; # ex: "Level1.tscn"
export var player_set_loc = [0,0];
export var player_set_dir = "down";
export var level_lock = 0;
export var elevator_level = 2;

# check if player is currently on pad
var player_touching = false;
var is_active = false;


# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");

func _ready():
	# check what levels have been completed
	is_active = gl_SceneLoader.completed_level >= level_lock;
	if is_active:
		$AnimatedSprite.play("on");
	else:
		$AnimatedSprite.play("off");

# Called every frame. 'delta' is the elapsed time since the previous frame.
var call_once = true;
func _process(delta):
	# if down is pressed and player is touching pad
	if Input.is_action_pressed("player_down") and player_touching and is_active and call_once:
		# if level is tutorial
		if gl_SceneLoader.completed_level <= -1:
			gl_SceneLoader.completed_level = 0;
		call_once = false;
		gl_SceneLoader.elevator_level = elevator_level;
		gl_SceneLoader.player_loc = player_set_loc;
		gl_SceneLoader.player_grav_dir = player_set_dir;
		print("Teleporter set player location: " + str(gl_SceneLoader.player_loc));
		gl_SceneLoader.anim_then_preloader(base_search_path + level_name); # load by singleton

# signals when player gets on and off the pad
func _on_Area2D_body_entered(body):
	if body.name == "Player":
		player_touching = true;
func _on_Area2D_body_exited(body):
	if body.name == "Player":
		player_touching = false;
