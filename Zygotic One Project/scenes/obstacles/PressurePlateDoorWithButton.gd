extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var open = false
export var OpenDelay = 0

var touched = false
var time_since_touched = 0
var play_once = false
# Called when the node enters the scene tree for the first time.
func _ready():
	if open:
		$AnimatedSprite.animation = "open"
		$DoorHitbox/DoorHitboxShape.disabled = true
	else:
		$AnimatedSprite.animation = "closed"
		$DoorHitbox/DoorHitboxShape.disabled = false
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	if open and not play_once:
		$AnimatedSprite.play("opening", false)
		$Door_SFX.play()
		play_once = true
	return
	
func _on_Door_body_entered(body):
	if body.name == "Player":
		time_since_touched = 0
		touched = true
		open = not open
	pass # Replace with function body.


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "opening":
		$AnimatedSprite.play("open")
		$DoorHitbox/DoorHitboxShape.disabled = true
	pass # Replace with function body.
