extends KinematicBody2D

export var Amplitude = 5 * 64 # Pixels
export var Frequency = 0.1 # Hertz
export var Phase = 0.0 # Radians
export var Anti = false
var initial_position = Vector2(0, 0)
var player = null
const IS_MOVING_PLATFORM = true

var t = 0
var velocity = Vector2(0, 0)

func find_player_recursive(node):
	var p = null
	for child in node.get_children():
		if child.get_name() == "Player":
			return child
		else:
			p = find_player_recursive(child)
			if p != null:
				return p
	return null

func find_player():
	var root = get_tree().root.get_child(0)
	return find_player_recursive(root)

func _ready():
	initial_position = position
	player = find_player()
	#if Anti:
	#	$Sprite.visible = false
	#	$Anti.visible = true
	#else:
	#	$Sprite.visible = true
	#	$Anti.visible = false
	var right = Vector2(cos(rotation), -sin(rotation))
	position = initial_position + right * Amplitude * sin(2 * PI * Frequency * t + Phase)
	return
	
func _physics_process(delta):
	#if Anti and not (Input.is_action_pressed("Time_Reverse") and player.time_reversal_left > 0):
	#	velocity = Vector2(0, 0)
	#	return
	var right = Vector2(cos(rotation), -sin(rotation))
	position = initial_position + right * Amplitude * sin(2 * PI * Frequency * t + Phase)
	t += delta
	velocity = (initial_position + right * Amplitude * sin(2 * PI * Frequency * t + Phase)) - position
	if get_parent() != null:
		velocity = velocity.rotated(get_parent().rotation)
	pass
