extends Node2D

# get hook to SaveState node
onready var gl_SaveState = get_node("/root/SaveState");

# death dialogue file
export (Array, String) var death_dialogues;
var death_dialogue_index = 0;
var dialogue_path = "res://dialogue/Death/"

# player finding code
func find_player_recursive(node):
	var p = null
	for child in node.get_children():
		if child.get_name() == "Player":
			return child
		else:
			p = find_player_recursive(child)
			if p != null:
				return p
	return null

func find_player():
	var root = get_tree().get_root();
	return find_player_recursive(root)

# do a thing when dead
var death_lock = false;
func player_death():
	# if it is the last level, don't play
	if last_loaded_scene == "res://scenes/Worlds/Crust/Level5_MiniBoss.tscn":
		return;
	
	# skip if the lock was already triggered
	if death_lock:
		return;
	death_lock = true;
	
	# play dialogue
	$Dialogue.reset();
	$Dialogue.play(dialogue_path + death_dialogues[death_dialogue_index]);
	death_dialogue_index += 1;
	if death_dialogue_index >= death_dialogues.size():
		death_dialogue_index = 0;
	
	# increment death counter?
	player_deaths += 1;

# play dialogue box when a player collects 50 coins
var pause_lock = false;
func coinDialogue():
	# play dialogue
	$Dialogue.reset();
	$Dialogue.play("res://dialogue/Time Bar Extension/Time_Bar_Extension.txt");
	
	# freeze player
	global_pause = true;
	pause_lock = true;

# saves completed levels
var levels_filename = "user://level_state.txt";
func saveLevels():
	# open file
	var file = File.new();
	file.open(levels_filename, File.WRITE);
	
	 # construct save string
	var out_str = "";
	out_str += str(completed_level) + " ";
	out_str += str(last_completed_level) + " ";
	out_str += str(player_deaths) + " ";
	file.store_string(out_str);
	file.close();

# loads completed levels
func loadLevels():
	# open file
	var file = File.new();
	
	# create new file if it's not there
	if not file.file_exists(levels_filename):
		file.open(levels_filename, File.WRITE);
		file.store_string("-1 -1 0");
		file.close();
	
	# load file
	file.open(levels_filename, File.READ);
	
	# read string 
	var line = file.get_line();
	var nums = line.split(' ', false, 0);
	completed_level = int(nums[0]);
	last_completed_level = int(nums[1]);
	player_deaths = int(nums[2]);
	
	# set hud visibility
	if completed_level > -1:
		hud_visibility = 2;
	
	# debug values
	print("Completed Level: " + str(completed_level));
	print("Last Completed Level: " + str(last_completed_level));
	print("Player Deaths: " + str(player_deaths));

# returns the sign of a number
func sign(num):
	if num > 0:
		return 1;
	if num < 0:
		return -1;
	return 0;

# track which levels the player has completed
# and trigger dialogue boxes based on it
# these are set by the end of level objects
var completed_level = 0; # tutorial doesn't count
var last_completed_level = -1; # this will trigger on tutorial completion

# triggers HUD visibility level
var hud_visibility = 2;

# tracks number of deaths
var player_deaths = 0;

# sets a player location
var player_loc = [13142, 7420]; # default position for probe scene

# sets player gravity
var player_grav_dir = "down";

# sets elevator location
var elevator_level = 0;
var elevator_floors = [0, 2815, 5630];

# target scene
var next_level = null;
var next_scene_name;

# target volume
var curr_volume = -3.0;
var target_volume = curr_volume;
var step_size = 0.2;
var step_time = 0.005; # seconds

# global pause for menu
var global_pause = false;

# reset for last level dialogue boxes
var final_dialogue_boxes = true;

var dialogue_is_playing = false;

# on ready
func _ready():
	loadLevels();
	pass;

# does ongoing interpolation
func _process(_delta):
	if curr_volume != target_volume:
		# step towards target
		var diff = target_volume - curr_volume;
		var step = step_size * (_delta / step_time);
		
		# hard set
		if abs(step) >= abs(diff):
			curr_volume = target_volume;
		else:
			# make a step
			curr_volume += step * sign(diff);
			
		# set background music volume
		AudioServer.set_bus_volume_db(3, curr_volume);
		
# plays the player teleporter animation before triggering the next scene
var cached_scene = "";
func anim_then_preloader(next_scene):
	# play animation, wait 1 second, then go to preloader
	var player = find_player();
	if player != null:
		player.play_teleport_out();
	$Timer2.wait_time = 1.0;
	$Timer2.start();
	cached_scene = next_scene;
	

# changes scene
func changeScene(scene_name):
	# load scene
	print("Changing to Scene: " + scene_name);
	var scene = load(scene_name);
	var level = scene.instance();
	
	# set HUD
	if completed_level > -1:
		hud_visibility = 2;
	
	# clear out root
	var root = get_tree().get_root();
	var children = root.get_children();
	for node in children:
		# whitelist the global singleton scenes
		if node.name != "SceneLoader":
			root.remove_child(node);
	
	# replace
	root.add_child(gl_SaveState);
	root.add_child(level);
	
	# undo any pause
	global_pause = false;

# change to next scene
func toNextScene():
	changeScene(next_scene_name);

# start screen scene change
func fromStartScene():
	# go to tutorial if last level completed is -1
	var next_scene = "None";
	if last_completed_level == -1:
		next_scene = "res://scenes/Worlds/Tutorial_Test.tscn";
	else:
		# go to probe
		next_scene = "res://scenes/Worlds/Probe.tscn";
	
	# call preloader
	toPreloader(next_scene);
		

# change to a preloader scene
var last_loaded_scene = "";
func toPreloader(next_scene):
	# update save state
	SaveState.saveLevels();
	saveLevels();
	
	# set scene name
	next_scene_name = next_scene;
	last_loaded_scene = next_scene;
	
	# load preloader first
	print("Going to Preloader");
	changeScene("res://scenes/Preloader.tscn");
	
	# set timer
	$Timer.wait_time = 1.0;
	$Timer.start();
	
	# reset death lock
	death_lock = false;
	
	# clear checkpoints
	checkpoint_active = false;
	checkpoint_loc = [0,0];

func _on_Timer_timeout():
	print("Timer Triggered!");
	toNextScene();
	$Timer.stop();

# do a thing when dialogue finishes
func _on_Dialogue_dialogue_finished():
	# reload current level
	print("Dialogue Finished Signal");
	$Dialogue.reset();

	# only reload if this wasn't triggered by the 50 coin thing
	if not global_pause and death_lock:
		reloadAtCheckpoint();
	
	# reset flags
	death_lock = false;
	global_pause = false;
	pause_lock = false;

func _on_Timer2_timeout():
	print("Timer2 Triggered!");
	$Timer2.stop();
	toPreloader(cached_scene);

# checkpoint management
var checkpoint_active = false;
var checkpoint_loc = [0,0];
func activateCheckpoint(loc):
	checkpoint_active = true;
	checkpoint_loc = loc;

# reload level with checkpoint location
func reloadAtCheckpoint():
	# reload level
	# toPreloader(last_loaded_scene);
	print("Reloading At Checkpoint");
	changeScene(last_loaded_scene);
	
	# check for checkpoint
	if checkpoint_active:
		var player = find_player();
		player.position = checkpoint_loc;

# audio slider volume hack
var music_volume = -40;
var sfx_volume = -40;
var pause_menu_buttons = 0;

	
	
	
	
	
	
	
	
	
	
	
	
	
	
