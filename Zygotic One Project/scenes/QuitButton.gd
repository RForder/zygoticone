extends Button

func _on_QuitButton_pressed():
	if not get_parent().get_node("CreditsScreen").visible:
		SaveState.saveLevels();
		get_tree().quit();
