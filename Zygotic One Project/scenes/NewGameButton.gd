extends Button

# turn on confirmation buttons and menu
# turn off continue button "StartButton"
func _on_NewGameButton_pressed():
	if not get_parent().get_node("CreditsScreen").visible:
		get_node("/root/StartScreen/StartButton").visible = false;
		get_node("/root/StartScreen/OS_new_game_confirmation").visible = true;
		get_node("/root/StartScreen/NewGameYes").visible = true;
		get_node("/root/StartScreen/NewGameNo").visible = true;
