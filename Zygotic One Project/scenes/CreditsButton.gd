extends Button

func _on_CreditsButton_pressed():
	if not get_parent().get_node("CreditsScreen").visible:
		get_parent().get_node("CreditsScreen").visible = true;
