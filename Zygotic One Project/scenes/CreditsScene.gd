extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	# start the animation
	$Full.play();
	
	# start the timer
	$Timer.start();

func _on_Timer_timeout():
	SceneLoader.toPreloader("res://scenes/StartScreen.tscn");
