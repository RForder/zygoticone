extends Area2D

# How to use:
# Create an instance of this in scene
# Set the play_after_level variable
# Set the full_freeze variable

# load dialogue box
var DiagBox = preload("res://temp_storage/DialogueScreen.tscn");
var diagInst = DiagBox.instance();
export var fileName = "res://dialogue/Tutorial/Tutoral_Level_End.txt"; # default file

# should player completely freeze?
export var full_freeze = true;

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");
# onready var gl_SaveState = get_node("/root/SaveState");

# what level do I play after?
export var play_after_level = 0;

# Called when the node enters the scene tree for the first time.
func _ready():
	# attach diag to parent
	diagInst.name = "Dbox";
	add_child(diagInst);
	
	# check if this level has already been completed
	if gl_SceneLoader.completed_level != gl_SceneLoader.last_completed_level:
		# check if the completed level is the one we're looking for
		if gl_SceneLoader.completed_level == play_after_level:
			# update last_completed_levels
			gl_SceneLoader.last_completed_level = play_after_level;
			
			# save completed level
			# gl_SaveState.saveLevels();
			gl_SceneLoader.saveLevels();
			
			# freeze player and play dialogue
			if full_freeze:
				get_tree().get_root().find_node("Player", true, false).player_freeze();
			else:
				get_tree().get_root().find_node("Player", true, false).player_freeze_with_anim();
			diagInst.play(fileName);
