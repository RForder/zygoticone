extends Button



func _on_ControlButton_pressed():
	if not get_parent().get_node("CreditsScreen").visible:
		$PauseMenu.setAllVisibility(not $PauseMenu.visible);
		$PauseMenu.setButtonVisibility(false);
