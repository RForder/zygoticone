extends VideoPlayer

# replays the video when it stops
func _on_VideoPlayer_finished():
	self.play();
