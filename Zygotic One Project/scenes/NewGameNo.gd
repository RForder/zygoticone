extends Button

# turn off confirmation menu stuff
func _on_NewGameNo_pressed():
	get_node("/root/StartScreen/StartButton").visible = true;
	get_node("/root/StartScreen/OS_new_game_confirmation").visible = false;
	get_node("/root/StartScreen/NewGameYes").visible = false;
	get_node("/root/StartScreen/NewGameNo").visible = false;

