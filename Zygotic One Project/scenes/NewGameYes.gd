extends Button

# turn off confirmation screen and buttons
func _on_NewGameYes_pressed():
	# change display stuff
	get_node("/root/StartScreen/StartButton").visible = true;
	get_node("/root/StartScreen/OS_new_game_confirmation").visible = false;
	get_node("/root/StartScreen/NewGameYes").visible = false;
	get_node("/root/StartScreen/NewGameNo").visible = false;
	
	# dump save data
	# level state
	SceneLoader.completed_level = -1;
	SceneLoader.last_completed_level = -1;
	SceneLoader.player_deaths = 0;
	SceneLoader.saveLevels();
	
	# coins
	SaveState.collected_coins = [[],[],[],[],[]];
	SaveState.saveLevels();
