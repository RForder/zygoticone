extends Area2D

# How to use:
# Create an instance of this in scene
# Set the filename in the inspector to the filepath of the dialogue
# Change the size of the collision box by changing the scale
# (scale is under DialogueTriggerOnEnter -> Node2D -> Transform)
# Move the trigger by clicking and draging on the object in scene view

# load dialogue box
var DiagBox = preload("res://temp_storage/DialogueScreenSpecial.tscn");
var diagInst = DiagBox.instance();
export var fileName = "res://dialogue/Tutorial/Tutoral_Level_End.txt"; # default file
export var final_level = false;
var play_once = true;

# should player completely freeze?
export var full_freeze = true;

# what should I set the HUD visibility to? 
# 0 = nothing, 1 = just time reversal bar, 2 = everything
export var set_hud_visibililty = 2;

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");

# Called when the node enters the scene tree for the first time.
func _ready():
	# attach diag to parent
	diagInst.name = "Dbox";
	add_child(diagInst);
	
	# trigger worm sprite
	if final_level and not SceneLoader.final_dialogue_boxes:
		$DianeWormIn.frame = 5;
	
# triggered when the player touches the collision shape
func _on_DialogueTriggerOnEnter_body_entered(body):
	# guard against replay on final level
	if not final_level or SceneLoader.final_dialogue_boxes:
		if play_once and body.name == "Player":
			# set hud visibility
			gl_SceneLoader.hud_visibility = set_hud_visibililty;
			print("Set HUD Visibility to " + str(gl_SceneLoader.hud_visibility));
			# choose whether or not it's a full freeze
			if full_freeze:
				get_tree().get_root().find_node("Player", true, false).player_freeze();
			else:
				get_tree().get_root().find_node("Player", true, false).player_freeze_with_anim();
			play_once = false;
			diagInst.play(fileName);
