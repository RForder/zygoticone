extends Button

# grab SceneLoader
onready var gl_SceneLoader = get_node("/root/SceneLoader");

# level to go to
export var level = "res://scenes/Worlds/Crust/Level1.tscn";

func _ready():
	self.connect("pressed", self, "_on_Button_pressed");
	print("Button connected to self");

func _process(delta):
	if SceneLoader.pause_menu_buttons == 0:
		self.visible = false;

func _on_Button_pressed():
	print("Pressed BUTTON");
	gl_SceneLoader.toPreloader(level);

#		gl_SceneLoader.elevator_level = elevator_level;
#		gl_SceneLoader.player_loc = player_set_loc;
#		gl_SceneLoader.player_grav_dir = player_set_dir;
#		gl_SceneLoader.anim_then_preloader(base_search_path + level_name); # load by singleton
