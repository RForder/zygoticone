extends Control

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");

# loop through everything since godot is annoying
func setAllNodes(node):
	if not node is CanvasLayer and not node is AudioStreamPlayer:
		node.visible = visible;
	for N in node.get_children():
		setAllNodes(N);
		# if N.get_child_count() > 0:
		# 	setAllNodes(N);

# toggle visibility externally
func setAllVisibility(is_visible):
	print("Visiblity Set");
	visible = is_visible;
	setAllNodes(self);

# toggle visibility
func _input(event):
	if event.is_action_pressed("Exit") and not gl_SceneLoader.pause_lock:
		visible = not visible;
		setAllNodes(self);
		gl_SceneLoader.global_pause = visible;

# start invisible
func _ready():
	visible = false;
	setAllNodes(self);
	gl_SceneLoader.global_pause = false;
	
# set buttons visible or invisible
func setButtonVisibility(is_visible):
	$CanvasLayer/TextureRect/Probe.visible = is_visible;
	$CanvasLayer/TextureRect/MainMenu.visible = is_visible;
