extends Area2D

# teleport to this level when player touches this collider
export var base_path = "res://scenes/Worlds/";
export var level = "Probe.tscn";
export var player_set_loc = [0,0];
export var player_set_dir = "down";
export var my_level_complete = 0;
export var set_elevator_level = 0;

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");

func _on_Teleport_on_Collider_body_entered(body):
	if body.name == "Player":
		# set elevator level
		gl_SceneLoader.elevator_level = set_elevator_level;
		
		# set player location and player gravity dir
		gl_SceneLoader.player_loc = player_set_loc;
		gl_SceneLoader.player_grav_dir = player_set_dir;
		print("Set SceneLoader.player_loc: " + str(player_set_loc));
		
		# update the last completed level if higher
		if my_level_complete > gl_SceneLoader.completed_level:
			gl_SceneLoader.completed_level = my_level_complete;
		
		# load next level
		gl_SceneLoader.anim_then_preloader(base_path + level); # load by singleton
