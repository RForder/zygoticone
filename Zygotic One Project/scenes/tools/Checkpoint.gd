extends Node2D

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");

# animation var
var active = false;

func _on_Area2D_body_entered(body):
	if body.name == "Player" and not active:
		# activate
		active = true;
		$AnimatedSprite.play("FlipOn");
		$CheckpointSFX.play()
		# trigger checkpoint management
		gl_SceneLoader.activateCheckpoint(position);


func _on_Area2D_body_exited(body):
	if body.name == "Player" and not active:
		# activate
		active = true;
		$AnimatedSprite.play("FlipOn");
		$CheckpointSFX.play()
		# trigger checkpoint management
		gl_SceneLoader.activateCheckpoint(position);
