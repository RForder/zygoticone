extends Label

# front fill with zeros
func frontFill(string, length):
	while len(string) < length:
		string = "0" + string;
	return string;

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	self.text = "#" + frontFill(str(SceneLoader.player_deaths), 5);
	print("Death Counter Text: " + str(self.text));


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# update the text
	self.text = "#" + frontFill(str(SceneLoader.player_deaths), 5);
	# print("DEATH COUNTER TEXT: " + str(self.text));
