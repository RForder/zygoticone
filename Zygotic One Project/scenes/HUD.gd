extends Node2D


# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");

# hook to player coins
var player = null;
var bar_index = 0;
var bar_width = 0;

# reverse bar sprites
export (Array, AtlasTexture) var bar_containers;
export (Array, AtlasTexture) var bar_insides;
export (Array, int) var bar_widths;

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# grab player from gl_SceneLoader
	if player == null:
		player = gl_SceneLoader.find_player();
		
	# update reverseBar
	bar_index = int(SaveState.coin_count / player.COINS_PER_INCREMENT);
	if bar_index >= bar_containers.size():
		bar_index = bar_containers.size() - 1;
	$ReverseContainer.texture = bar_containers[bar_index];
	$ReverseBar.texture = bar_insides[bar_index];
	bar_width = bar_widths[bar_index];
	
	
	# reset visibility
	$CoinSprite.visible = true;
	$CoinLabel.visible = true;
	$KeySprite.visible = true;
	$KeyLabel.visible = true;
	$ReverseBar.visible = true;
	$ReverseContainer.visible = true;
	$Underlay.visible = true;
	# check visibility
	if gl_SceneLoader.hud_visibility < 2:
		$CoinSprite.visible = false;
		$CoinLabel.visible = false;
		$KeySprite.visible = false;
		$KeyLabel.visible = false;
		$Underlay.visible = false;
	if gl_SceneLoader.hud_visibility < 1:
		$ReverseBar.visible = false;
		$ReverseContainer.visible = false;

