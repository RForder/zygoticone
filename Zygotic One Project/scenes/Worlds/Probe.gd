extends Node2D

# only move while player is standing in area
var is_active = false;

# declare vars
var lerp_spd = 5.0;

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");


# Called when the node enters the scene tree for the first time.
var tmr = Timer.new()
func _ready():
	# hide mouse cursor when inactive
	tmr.one_shot = true
	tmr.wait_time = 1.2
	tmr.connect("timeout", self, "hide_cursor")
	add_child(tmr)
	
	# disable time reverse
	gl_SceneLoader.hud_visibility = 0;
	gl_SceneLoader.pause_menu_buttons = 2;
	
	# get the elevator position
	var floor_index = gl_SceneLoader.elevator_level;
	var floors = gl_SceneLoader.elevator_floors;
	
	# get the player position
	print("Probe: Player Position Before: " + str(gl_SceneLoader.player_loc));
	var xpos = gl_SceneLoader.player_loc[0];
	var ypos = gl_SceneLoader.player_loc[1];
	ypos += floors[floor_index];
	
	# get the player and move them
	$Player.position.x = xpos;
	$Player.position.y = ypos + 100;
	print("Moved Player to Loc: " + str($Player.position));
	
	# set the player gravity
	$Player.setGravity(gl_SceneLoader.player_grav_dir);
	print("Player Direction Set: " + str(gl_SceneLoader.player_grav_dir));
	
	# set player animation
	$Player.player_unfreeze();

# hide mouse cursor when inactive
func _input(e: InputEvent):
	if e is InputEventMouseMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		tmr.start()
		
func hide_cursor():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
