extends Node2D

const CAMERA_ZOOM = 0.49
var player = null;
var tmr = Timer.new()

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");

# Called when the node enters the scene tree for the first time.
func _ready():
	# hide mouse cursor when inactive
	tmr.one_shot = true
	tmr.wait_time = 1.2
	tmr.connect("timeout", self, "hide_cursor")
	add_child(tmr)
	
	gl_SceneLoader.pause_menu_buttons = 0;
	gl_SceneLoader.hud_visibility = 0;
	player = find_node("Player", true, false);
	assert(player != null);
	pass;

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	$WorldContainer.get_material().set_shader_param("reversing", Input.is_action_pressed("Time_Reverse") and player.time_reversal_left > 0 and gl_SceneLoader.hud_visibility >= 2)
	$HUD/CoinLabel.text = str(SaveState.coin_count);
	$HUD/KeyLabel.text = str(player.keys);
	
	# calculate percentage for remaining player reversal
	# HUD stuff should be done with a singleton instead of this nonesense
	# $Bar.texture.set_region(Rect2(0, 0, img_width * percent ,125));
	var BAR_WIDTH = $HUD.bar_width;
	var BAR_HEIGHT = 125;
	var percent = float(player.time_reversal_left) / float(player.current_max_time_reversal);
	$HUD/ReverseBar.texture.set_region(Rect2(0, 0, BAR_WIDTH * percent, BAR_HEIGHT));
	# $HUD/TimeReversalBar.set_size(Vector2(player.time_reversal_left * 1820 / player.MAX_TIME_REVERSAL, 40))
	pass

# hide mouse cursor when inactive
func _input(e: InputEvent):
	if e is InputEventMouseMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		tmr.start()
		
func hide_cursor():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
