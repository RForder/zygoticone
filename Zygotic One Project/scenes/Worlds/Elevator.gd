extends Node2D

# only move while player is standing in area
var is_active = false;

# declare vars
var lerp_spd = 5.0;

# floor levels
var floor_index = 0;
var floors = [];

# time between presses
export var wait_time = 0.5;
var wait_timer = 0;

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");


# Called when the node enters the scene tree for the first time.
func _ready():
	# grab initial level and floors
	floor_index = gl_SceneLoader.elevator_level;
	floors = gl_SceneLoader.elevator_floors;
	position = Vector2(0, floors[floor_index]);
	pass # Replace with function body.

func _input(event):
	if is_active and wait_timer >= wait_time:
		if event is InputEventKey and event.pressed:
			# process keys
			if Input.is_action_just_pressed("player_up"):
				floor_index += 1;
				wait_timer = 0;
				$Elevator_SFX.play()
			if Input.is_action_just_pressed("player_down"):
				floor_index -= 1;
				wait_timer = 0;
				$Elevator_SFX.play()
				
			# clamp position
			if floor_index < 0:
				floor_index = 0;
			if floor_index >= floors.size():
				floor_index = floors.size() - 1;
			

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# lerp towards position
	position = position.linear_interpolate(Vector2(0, floors[floor_index]), delta * lerp_spd);
	
	# update timer
	wait_timer += delta;


func _on_ActivationArea_body_entered(body):
	if body.name == "Player":
		is_active = true;


func _on_ActivationArea_body_exited(body):
	if body.name == "Player":
		is_active = false;
