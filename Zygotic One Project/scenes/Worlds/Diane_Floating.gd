extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var float_dist = 10.0;
export var cycle_time = 4.0; # seconds

var timer = 0;
var start_pos;
var dir = 1;


# Called when the node enters the scene tree for the first time.
func _ready():
	start_pos = position;


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# update timer
	timer += delta * dir;
	
	# change timer direction
	if timer >= cycle_time:
		dir = -1;
	if timer <= 0:
		dir = 1;
	
	# move sprite
	position = start_pos;
	position.y -= (timer / cycle_time) * float_dist;














