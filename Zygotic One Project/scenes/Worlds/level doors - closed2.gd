extends Sprite

# Called when the node enters the scene tree for the first time.
func _ready():
	if SceneLoader.completed_level >= 4:
		self.visible = false;
		$KinematicBody2D/CollisionShape2D.disabled = true;
	pass # Replace with function body.
