extends Control;
signal dialogue_over;

# link to special sprites
var diane_sprite;
var worm_sprite;

# sets the target conversation file
func setFile(name):
	readFromFile(name);

# resets dialogue box
func reset():
	play_once = true;
	pause = false;
	self.visible = false;
	diag_index = 0;

# starts the object
func play():
	if play_once:
		play_once = false;
		pause = true;
		self.visible = true;
		$OpenSound.play();
		load_dialogue();

# hide objects
func hideAll():
	left.visible = false;
	right.visible = false;
	self.visible = false;
	$EndIndicator.visible = false;

# chops items by delimiter
func splitItems(line, delimeter):
	# flush whitespace
	line = line.replace(' ', '');
	return Array(line.split(delimeter));

# chops out brackets
var _chopBrackets_ret; # holy crap Godot. why?
func chopBrackets(line):
	# find brackets
	var lbrac = line.find('[') + 1;
	var rbrac = line.find(']');
	var dist = rbrac - lbrac;
	
	# cut out the line
	var cut_line = line.substr(lbrac, dist);
	_chopBrackets_ret = line.substr(rbrac + 2, line.length() - 2); # plus one to equal, plus one to skip
	return cut_line;

# creates a class structure for dialogue lines
class Dialogue:
	# declare vars
	var character;
	var sprite;
	var lines;
	var side; # 1 = right, -1 = left
	var anim;
	
	# init empty
	func _init(_character = null, _sprite = null, _lines = "", _side = 0, _anim = null):
		self.character = _character;
		self.sprite = _sprite;
		self.lines = _lines;
		self.side = _side;
		self.anim = _anim;
	
	# append lines
	func appendLines(_lines):
		self.lines += _lines;
	
	# checks if all values have been initialized
	func allReady():
		return self.character != null and self.sprite != null and self.lines.length() > 0;

# declare dialogue vars
var dialogue = [];
var diag_index = 0;
var line_finished = false;
var text_spd = 100.0; # characters per second
var line_time = 0; # seconds to play whole line
var play_once = true;
# var dialogue_class = load("res://dialogue.gd");

# get reference to left and right portraits
onready var left = $Left;
onready var right = $Right;
var char_dir = "res://sprites/Portraits/";

# outside control
var pause = false;

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");

# load file
func readFromFile(filename):
	# trigger dialogue flag
	gl_SceneLoader.dialogue_is_playing = true;
	
	# reset dialogue
	dialogue = [];
	
	# open file
	var file = File.new();
	file.open(filename, File.READ);

	# prepare character and expression tracking
	var default_expr = "Neutral1";
	var character = null;
	var expression = default_expr;
	
	# prepare character classes
	var temp_diag = Dialogue.new();
	
	# special case the first line (character declaration)
	# dump all until first line with brackets
	var line = file.get_line();
	while line.find('[') == -1:
		line = file.get_line();
	
	# split the line
	var left_line = chopBrackets(line);
	line = _chopBrackets_ret;
	var right_line = chopBrackets(line);
	line = _chopBrackets_ret;
	
	# parse into character portraits
	var _left_chars = splitItems(left_line, ',');
	var right_chars = splitItems(right_line, ',');
	
	# go through line by line
	while not file.eof_reached():
		line = file.get_line();
		if line.length() > 0 and line[0] != "#": # skip empty lines and comments
			# chop new line characters
			# line = line.substr(0, line.length()-1);
			
			# look for new expression line
			if line[0] == "[":
				# create a new lines
				if temp_diag.allReady():
					dialogue.append(temp_diag);
				temp_diag = Dialogue.new();
				
				# chop off brackets
				line = chopBrackets(line);
				
				# split into character and expression
				var slice = splitItems(line, ',');
				var new_char = slice[0];
				var new_expr = slice[1];
				
				# check against old
				if new_char.length() > 0 and new_char != character:
					character = new_char;
					expression = default_expr;
				if new_expr.length() > 0 and new_expr != expression:
					expression = new_expr;
				
				# add to dialogue
				temp_diag.character = character;
				temp_diag.sprite = character + "_" + expression;
				temp_diag.side = -1;
				if right_chars.find(character) != -1:
					temp_diag.side = 1;
			# check for new animation line
			elif line[0] == "$":
				temp_diag.anim = line.substr(1, len(line)-1);
				print("Dialogue Animation: " + str(temp_diag.anim));
			else:
				# add to lines
				temp_diag.lines += " " + line;
		# endif line.length() > 0
	# endwhile not file.eof
	
	# add the last line
	if temp_diag.allReady():
		dialogue.append(temp_diag);

# start up the dialogue func
func _ready():
	# link to special sprites
	print("Parent: " + str(get_parent().name));
	print("Parent Parent: " + str(get_parent().get_parent().name));
	diane_sprite = get_parent().get_parent().get_node("DianeTeleportIn");
	worm_sprite = get_parent().get_parent().get_node("DianeWormIn");
	print("Diane Sprite: " + str(diane_sprite.name));
	
	# hide
	hideAll();
	
	# start animation
	$EndIndicator/AnimationPlayer.play("Bounce");

# plays out the dialogue
func load_dialogue():
	# set text
	if diag_index < dialogue.size():
		# start sound
		$TalkSound.play();
		#AudioServer.set_bus_effect_enabled(3, 1, true);
		if diag_index == 0:
			gl_SceneLoader.target_volume = -9;
		# calculate line time
		var diag = dialogue[diag_index];
		line_time = float(diag.lines.length()) / text_spd;
		
		# start animation if available # if teleportIn, offsety = -39
		if diag.anim != null:
			if diag.anim == "DianeWormIn":
				worm_sprite.visible = true;
				worm_sprite.play(diag.anim);
			else:
				if diag.anim == "DianeTeleportIn":
					diane_sprite.offset = Vector2(0, -39);
				else:
					diane_sprite.offset = Vector2(0, 0);
				diane_sprite.visible = true;
				diane_sprite.play(diag.anim);
		
		# show portrait
		var speaker = null;
		left.visible = false;
		right.visible = false;
		#if diag.side > 0:
		#	speaker = right;
		#else:
		#	speaker = left;
		speaker = right; # hard set to right for now
		speaker.visible = true;
		
		# set default portrait
		var sprite_path = char_dir + "Debug.png";
		
		# set portrait sprite
		var base_str = char_dir;
		base_str += diag.character + "/";
		base_str += diag.sprite + ".png";
		if ResourceLoader.exists(base_str):
			sprite_path = base_str;
		else:
			print("Not Found: " + base_str);
		speaker.texture = load(sprite_path);
		print("Portrait: " + str(sprite_path));
		
		# draw text
		line_finished = false;
		$RichTextLabel.percent_visible = 0;
		$RichTextLabel.bbcode_text = diag.lines;
		$Tween.interpolate_property(
			$RichTextLabel, "percent_visible", 0, 1, line_time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
		);
		$Tween.start();
	else:
		# delete this box
		emit_signal("dialogue_over");
		if get_tree().get_root().find_node("Player", true, false) != null:
			get_tree().get_root().find_node("Player", true, false).player_unfreeze();
		# get_parent().queue_free();
		pause = false;
		self.visible = false;
		gl_SceneLoader.target_volume = -3.0;
		gl_SceneLoader.dialogue_is_playing = false;
	# increment text
	diag_index += 1;

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	# if not playing hide everything
	if not pause:
		hideAll();
	else:
		# icon showing 
		$EndIndicator.visible = line_finished;
		# look for line finished
		if line_finished:
			$TalkSound.stop();
			# look for "enter"
			if Input.is_action_just_pressed("ui_accept"):
				load_dialogue();
				$EndDialogueSound.play();
		else:
			# no interupt
			# check for interrupt input
#			if Input.is_action_just_pressed("ui_accept"):
#				line_finished = true;
#				$Tween.stop($RichTextLabel, "percent_visible");
#				$RichTextLabel.percent_visible = 1.0;
			pass;

# from the tweening node on DialogueBox
func _on_Tween_tween_completed(_object, _key):
	line_finished = true;
