extends CanvasLayer
signal dialogue_finished;

# resest dialogue box
func reset():
	var child = get_node("DialogueBox");
	child.reset();

# play convo # is deleted at end
func play(filename):
	var child = get_node("DialogueBox");
	child.setFile(filename);
	child.play();
	child.connect("dialogue_over", self, "_repeat_signal");

# replay signal
func _repeat_signal():
	emit_signal("dialogue_finished");
