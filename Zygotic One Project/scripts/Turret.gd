extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var minimum_rotation = -90
export var maximum_rotation = 90
var target_rotation = 0
var pointed_at_player = false
var player = null
export var MAXIMUM_DIST_TO_PLAYER = 3000;
var squared_max_dist = 0;

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");


func find_player_recursive(node):
	var p = null
	for child in node.get_children():
		if child.get_name() == "Player":
			return child
		else:
			p = find_player_recursive(child)
			if p != null:
				return p
	return null

func find_player():
	var root = get_tree().get_root();
	return find_player_recursive(root)
	
# Called when the node enters the scene tree for the first time.
func _ready():
	# get a player reference
	player = find_player()
	assert(player != null)
	
	# calculate the squared distance (cheaper to calculate)
	squared_max_dist = MAXIMUM_DIST_TO_PLAYER * MAXIMUM_DIST_TO_PLAYER;


func _physics_process(_delta):
	# global pause
	if gl_SceneLoader.global_pause:
		return;
		
	# check distance and early quit
	var dx = player.position.x - position.x;
	var dy = player.position.y - position.y;
	var dist_to_player = dx*dx + dy*dy;
	if dist_to_player < squared_max_dist:
		pointed_at_player = false;
		return;
	
	# behave normally
	target_rotation = (get_global_transform().get_origin() - player.get_global_transform().get_origin()).angle() - get_global_transform().get_rotation()
	while target_rotation < 0:
		target_rotation += 2 * PI
	target_rotation = fmod(target_rotation, 2 * PI)
	var minr = minimum_rotation + 90
	while minr < 0:
		minr += 360
	minr = minr * 2 * PI / 360
	var maxr = maximum_rotation + 90
	while maxr < 0:
		maxr += 360
	maxr = maxr * 2 * PI / 360
	if fmod(maxr - minr + 2 * PI, 2 * PI) > fmod(target_rotation - minr + 2 * PI, 2 * PI):
		$Cannon.rotation = target_rotation
		$Label.text = str(target_rotation)
		pointed_at_player = true
	else:
		pointed_at_player = false
	pass

func _on_Timer_timeout():
	if not player.dead and pointed_at_player and not gl_SceneLoader.global_pause:
		var rocket = load("res://scenes/obstacles/Rocket.tscn").instance()
		add_child(rocket)
		rocket.owner = self
		var dir = -Vector2(cos($Cannon.rotation), sin($Cannon.rotation))
		rocket.velocity = rocket.SPEED * dir
		rocket.position = $Cannon.position + 140 * dir
		$Turret_Fire_SFX.play()
	pass
