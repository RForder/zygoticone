extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

var break_once = true;
func _on_Door_body_entered(body):
	if body.name == "Player" and body.keys > 0 and break_once:
		break_once = false;
		body.keys = body.keys - 1
		$AnimatedSprite.play("opening")
		$Key_Door_SFX.play()
	pass # Replace with function body.


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "opening":
		$AnimatedSprite.play("open")
		$DoorHitbox/DoorHitboxShape.disabled = true
	pass # Replace with function body.
