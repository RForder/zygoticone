extends KinematicBody2D

# pause all controls
var FREEZE_CONTROLS = false;
var FULL_STOP = false;

func player_freeze_with_anim():
	FREEZE_CONTROLS = true;
func player_freeze():
	FULL_STOP = true;
	$AnimatedSprite.stop();
func player_unfreeze():
	FULL_STOP = false;
	FREEZE_CONTROLS = false;
	$AnimatedSprite.play();

# play player teleport animation
var LOCK_ANIM = false;
func play_teleport_out():
	player_freeze_with_anim();
	print("Playing Teleport Out");
	$AnimatedSprite.play("Teleport Out");
	$Player_SFX/Teleport_SFX.play()
	LOCK_ANIM = true;

	
# returns sign of number (-1, 0, 1)
func sign(num):
	if num > 0:
		return 1;
	if num < 0:
		return -1;
	return 0;

# Constants
const FRAMES_STUCK_TILL_DEATH = 15
const TERMINAL_SPEED = 65
const GRAVITY_MAGNITUDE = .47
const FALL_DEATH_SPEED = 100
const FALL_ANIMATION_SPEED = 38
const SLOW_LAND = 10
const FALL_VELOCITY_DAMPER = 0.85
const ABSORBED_VELOCITY_THRESHOLD = 0.01;
const WALK_SPEED = 7.5
const AIR_WALK_SPEED = 5
const WALK_DAMPER = 0.8
const MAX_HIST = 600 # 10 seconds
const SLIDE_THRESHOLD = .1
const GRAVITY_CHARGES = 2
const MAX_TIME_REVERSAL = 10 * 60
const TIME_BAR_INCREMENT = 50;
const COINS_PER_INCREMENT = 50;
const INITIAL_MAX_TIME_REVERSAL = MAX_TIME_REVERSAL - (TIME_BAR_INCREMENT * 4); # we increase 4 times
var current_max_time_reversal = INITIAL_MAX_TIME_REVERSAL;

export var InitialGravity = "down"

# get hook to scene loader
onready var gl_SceneLoader = get_node("/root/SceneLoader");

# Member variables
var intersections = 0
var stuck_frames = 0
var no_fall_death = 0
var sticking = false
var sticking_to = null
var time_reversal_left = current_max_time_reversal;
var keys = 0
var boss_keys = 0
var fall_velocity = Vector2(0, 0)
var walk_velocity = Vector2(0, 0)
var walk_speed = 0
var gravity_rot = 0
var sprite_rot = 0
var simulation_speed = 1
var on_ground = false
var gravity_charges_used = 0
var dead = false
var flip_h = false
var sprite_rotating = false
var last_grav = "down"
var collision_angle = 0
var one_shot_anim = false
var death_areas = 0


# History lists for time reversal
var dead_hist = []
var pos_hist = []
var rot_hist = []
var grav_hist = []
var fall_hist = []
var animation_hist = []
var flip_hist = []
var grav_cnt_hist = []
var last_grav_hist = []
var on_ground_hist = []

func set_player_animation(name, l_flip_h):
	# lock the animation
	if LOCK_ANIM:
		return;
	# check
	if LOCK_ANIM:
		print(name);
	# Don't play any animation except death when dead (ZS-237)
	if dead:
		name = "death"
	else:
		if (name == "gravity_change" or name == "landing" or name == "landing_slow"):
			one_shot_anim = true
		if one_shot_anim:
			if $AnimatedSprite.animation == "landing" and name != "walking" and name != "gravity_change":
				name = "landing"
			elif $AnimatedSprite.animation == "landing_slow" and name != "walking" and name != "gravity_change":
				name = "landing_slow"
			elif $AnimatedSprite.animation == "gravity_change" and name != "landing" and name != "landing_slow":
				name = "gravity_change"
	$AnimatedSprite.play(name)
	$AnimatedSprite.flip_h = l_flip_h
	animation_hist.insert(0, name)
	if animation_hist.size() > MAX_HIST:
		animation_hist.pop_back()
	flip_hist.insert(0, l_flip_h)
	if flip_hist.size() > MAX_HIST:
		flip_hist.pop_back()

func kill():
	if dead or Input.is_action_pressed("Time_Reverse") or sprite_rotating:
		return
	dead = true
	get_node("Player_SFX/Death_explosion").play()
	return

func setGravity(dir):
	print("Player Set Gravity Direction: " + str(dir));
	if dir == "up":
		gravity_rot = PI;
	if dir == "right":
		gravity_rot = 3 * PI / 2;
	if dir == "left":
		gravity_rot = PI / 2;
	if dir == "down":
		gravity_rot = 0;
	
	# update sprite and last_grav
	sprite_rot = gravity_rot;
	last_grav = dir;

func _ready():
	# update max time
	current_max_time_reversal = INITIAL_MAX_TIME_REVERSAL + (int(SaveState.coin_count / COINS_PER_INCREMENT) * TIME_BAR_INCREMENT);
	if current_max_time_reversal > MAX_TIME_REVERSAL:
		current_max_time_reversal = MAX_TIME_REVERSAL;
	time_reversal_left = current_max_time_reversal;
	
	# start facing left
	flip_h = true;
	
	var rot = 0
	setGravity(InitialGravity.to_lower());
	pass

var coin_timer = 0;
func _process(_delta):
	# update max time
	current_max_time_reversal = INITIAL_MAX_TIME_REVERSAL + (int(SaveState.coin_count / COINS_PER_INCREMENT) * TIME_BAR_INCREMENT);
	if current_max_time_reversal > MAX_TIME_REVERSAL:
		current_max_time_reversal = MAX_TIME_REVERSAL;
#
#	# dev shortcuts
#	if Input.is_key_pressed(KEY_U):
#		print("Freeze Controls: " + str(FREEZE_CONTROLS));
#		print("FULL STOP: " + str(FULL_STOP));
#		print("LOCK ANIM: " + str(LOCK_ANIM));
#		print("Gravity Rot: " + str(gravity_rot));
#		print("Dead: " + str(dead));
#	if Input.is_key_pressed(KEY_K):
#		gl_SceneLoader.reloadAtCheckpoint();
#	if Input.is_key_pressed(KEY_Z):
#		time_reversal_left = 0;
#	if Input.is_key_pressed(KEY_M) and coin_timer > 0.3:
#		coins += 50;
#		coin_timer = 0;
#	if Input.is_key_pressed(KEY_N) and coin_timer > 0.3:
#		coins -= 50;
#		coin_timer = 0;
#	coin_timer += _delta;
	
	# lower bound coins
	# if coins < 0:
	# 	coins = 0;
	
	# actual stuff
	# if Input.is_action_just_pressed("Exit"):
	# 	get_tree().quit()
	# if Input.is_action_just_pressed("Enter"):
	# 	if Input.is_action_pressed("Modifier"):
	# 		OS.window_fullscreen = !OS.window_fullscreen
	return

func _physics_process(delta):
	# see if a total freeze here will stop everything
	if FULL_STOP or gl_SceneLoader.global_pause:
		return;
	
	# if dead, trigger the death behavior in the global scene loader
	if dead and time_reversal_left <= 0:
		print("Player Calling player_death()")
		gl_SceneLoader.player_death();
		if gl_SceneLoader.last_loaded_scene == "res://scenes/Worlds/Crust/Level5_MiniBoss.tscn":
			position.x = -1;
	
	# continue on as normal
	if death_areas > 0 and not dead:
		kill()

	if $AnimatedSprite.animation != "falling_fast":
		$Player_SFX/Terminal_Velocity.stop()

	if intersections > 0 and not Input.is_action_pressed("Time_Reverse"):
		stuck_frames = stuck_frames + 1
		if stuck_frames > FRAMES_STUCK_TILL_DEATH:
			kill()
			stuck_frames = 0
	else:
		stuck_frames = 0
	

	# ZS-324 and ZS-323
	$CollisionShape2D.disabled = (Input.is_action_pressed("Time_Reverse") and time_reversal_left > 0 and gl_SceneLoader.hud_visibility >= 2) or dead
	if FREEZE_CONTROLS:
		$CollisionShape2D.disabled = false;

	#Audio for time reverse
	if Input.is_action_just_pressed("Time_Reverse") and time_reversal_left > 0 and gl_SceneLoader.hud_visibility >= 2:
		get_node("Player_SFX/Time_Reverse").play()
	if Input.is_action_just_released("Time_Reverse") or time_reversal_left <= 0 and gl_SceneLoader.hud_visibility >= 2:
		get_node("Player_SFX/Time_Reverse").stop()
		
	if Input.is_action_just_pressed("Time_Reverse") and time_reversal_left > 100 and gl_SceneLoader.hud_visibility >= 2:
		time_reversal_left = time_reversal_left - 10
	
	# Time reversal
	if Input.is_action_pressed("Time_Reverse") and time_reversal_left > 0 and not FREEZE_CONTROLS and gl_SceneLoader.hud_visibility >= 2:
		time_reversal_left = time_reversal_left - 1
		var d = dead_hist.pop_front()
		if d != null:
			dead = d
		var pos = pos_hist.pop_front()
		if pos != null:
			set_position(pos)
		var rot = rot_hist.pop_front()
		if rot != null:
			sprite_rot = rot
		var g = grav_hist.pop_front()
		if g != null:
			gravity_rot = g
		var f = fall_hist.pop_front()
		if f != null:
			fall_velocity = f
		walk_velocity = Vector2(0, 0)
		var a = animation_hist.pop_front()
		if a != null:
			$AnimatedSprite.play(a)
		var flip = flip_hist.pop_front()
		if flip != null:
			$AnimatedSprite.flip_h = flip
		var grav_cnt = grav_cnt_hist.pop_front()
		if grav_cnt != null:
			gravity_charges_used = grav_cnt
		var lg = last_grav_hist.pop_front()
		if lg != null:
			last_grav = lg
		var og = on_ground_hist.pop_front()
		if og != null:
			on_ground = og
		$AnimatedSprite.rotation = sprite_rot
		$CollisionShape2D.rotation = sprite_rot
		$Label.text = str(on_ground) + " " + str(gravity_charges_used)
		return
		
	# Stop moving when dead
	if dead:
		$AnimatedSprite.play("death")
		if time_reversal_left == 0:
			$Label.text = "Game Over"
		return

	# Change gravity rotation on key press
	if not dead and gravity_charges_used < GRAVITY_CHARGES and not FREEZE_CONTROLS:
		if last_grav != "up" and Input.is_action_just_pressed("Gravity_Up"):
			gravity_rot = PI
			gravity_charges_used += 1
			sprite_rotating = true
			last_grav = "up"
			$AnimatedSprite.frame = 0
			set_player_animation("gravity_change", flip_h)
			
			#Audio
			if gravity_charges_used == 1:
				get_node("Player_SFX/Gravity_Shift1").play()
			if gravity_charges_used == 2:
				get_node("Player_SFX/Gravity_Shift2").play()
			
			
		elif last_grav != "right" and Input.is_action_just_pressed("Gravity_Right"):
			gravity_rot = 3 * PI / 2
			gravity_charges_used += 1
			sprite_rotating = true
			last_grav = "right"
			$AnimatedSprite.frame = 0
			set_player_animation("gravity_change", flip_h)
			
			#Audio
			if gravity_charges_used == 1:
				get_node("Player_SFX/Gravity_Shift1").play()
			if gravity_charges_used == 2:
				get_node("Player_SFX/Gravity_Shift2").play()
			
		elif last_grav != "left" and Input.is_action_just_pressed("Gravity_Left"):
			gravity_rot = PI / 2
			gravity_charges_used += 1
			sprite_rotating = true
			last_grav = "left"
			$AnimatedSprite.frame = 0
			set_player_animation("gravity_change", flip_h)
			
			#Audio
			if gravity_charges_used == 1:
				get_node("Player_SFX/Gravity_Shift1").play()
			if gravity_charges_used == 2:
				get_node("Player_SFX/Gravity_Shift2").play()
			
		elif last_grav != "down" and Input.is_action_just_pressed("Gravity_Down"):
			gravity_rot = 0
			gravity_charges_used += 1
			sprite_rotating = true
			last_grav = "down"
			$AnimatedSprite.frame = 0			
			set_player_animation("gravity_change", flip_h)
			
			#Audio
			if gravity_charges_used == 1:
				get_node("Player_SFX/Gravity_Shift1").play()
			if gravity_charges_used == 2:
				get_node("Player_SFX/Gravity_Shift2").play()

	# Change sprite rotation to match gravity rotation (interpolate)
	# Rotate shortest distance to match gravity
	if (gravity_rot - sprite_rot > PI):
		sprite_rot += 2 * PI
	if (sprite_rot - gravity_rot > PI):
		gravity_rot += 2 * PI
	sprite_rot += (gravity_rot - sprite_rot) * delta * 8

	# Hack for ZS-113
	if sprite_rotating and abs(gravity_rot - sprite_rot) < 0.1:
		sprite_rotating = false
		on_ground = false
		
	$AnimatedSprite.rotation = sprite_rot
	$CollisionShape2D.rotation = sprite_rot
	$Area2D/CollisionShape2D.rotation = sprite_rot

	# Compute fall velocity
	var gravity = Vector2(0, 0)
	gravity.x = -sin(gravity_rot) * GRAVITY_MAGNITUDE
	gravity.y = cos(gravity_rot) * GRAVITY_MAGNITUDE
		
	fall_velocity += gravity

	# ZS-339
	if fall_velocity.length() > TERMINAL_SPEED:
		fall_velocity = fall_velocity.normalized() * TERMINAL_SPEED
	
	# Save state
	dead_hist.insert(0, dead)
	if (dead_hist.size() > MAX_HIST):
		dead_hist.pop_back()
	rot_hist.insert(0, sprite_rot)
	if (rot_hist.size() > MAX_HIST):
		rot_hist.pop_back()
	grav_hist.insert(0, gravity_rot)
	if (grav_hist.size() > MAX_HIST):
		grav_hist.pop_back()
	fall_hist.insert(0, fall_velocity)
	if (fall_hist.size() > MAX_HIST):
		fall_hist.pop_back()
	pos_hist.insert(0, position)
	if pos_hist.size() > MAX_HIST:
		pos_hist.pop_back()	
	grav_cnt_hist.insert(0, gravity_charges_used)
	if grav_cnt_hist.size() > MAX_HIST:
		grav_cnt_hist.pop_back()
	last_grav_hist.insert(0, last_grav)
	if last_grav_hist.size() > MAX_HIST:
		last_grav_hist.pop_back()
	on_ground_hist.insert(0, on_ground)
	if on_ground_hist.size() > MAX_HIST:
		on_ground_hist.pop_back()
		
	# Compute walk velocity
	var right_vector = Vector2(gravity.y, -gravity.x).normalized()
	var sprite_rot2 = fmod(sprite_rot, 2 * PI)
	
	
	var anim = null
	walk_speed = 0
	if dead:
		anim = "death"
	elif not on_ground:
		#threshhold for FALL_ANIMATION_SPEED to trigger falling animation
		if fall_velocity.length() > FALL_ANIMATION_SPEED - 3:
			anim = "falling_fast"
			if $AnimatedSprite.animation != anim:
				$Player_SFX/Terminal_Velocity.play()
		else:
			anim = "falling_idle"
	else:
		anim = "idle"
	
	if not dead and not FREEZE_CONTROLS:
		# Gravity is right
		if (sprite_rot2 > 5 * PI / 4 && sprite_rot2 <  7 * PI / 4):
			if Input.is_action_pressed("player_up"):
				if on_ground:
					walk_speed = WALK_SPEED
					anim = "walking"
				else:
					walk_speed = AIR_WALK_SPEED
				flip_h = false
			elif Input.is_action_pressed("player_down"):
				if on_ground:
					walk_speed = -WALK_SPEED
					anim = "walking"
				else:
					walk_speed = -AIR_WALK_SPEED
				flip_h = true
			else:
				walk_speed *= WALK_DAMPER
		# Gravity is down
		if (sprite_rot2 > 7 * PI / 4 || sprite_rot2 <  PI / 4):
			if Input.is_action_pressed("player_right"):
				if on_ground:
					walk_speed = WALK_SPEED
					anim = "walking"
				else:
					walk_speed = AIR_WALK_SPEED
				flip_h = false
			elif Input.is_action_pressed("player_left"):
				if on_ground:
					walk_speed = -WALK_SPEED
					anim = "walking"
				else:
					walk_speed = -AIR_WALK_SPEED
				flip_h = true
			else:
				walk_speed *= WALK_DAMPER
		# Gravity is up
		if (sprite_rot2 > 3 * PI / 4 && sprite_rot2 < 5 * PI  / 4):
			if Input.is_action_pressed("player_left"):
				if on_ground:
					walk_speed = WALK_SPEED
					anim = "walking"
				else:
					walk_speed = AIR_WALK_SPEED
				flip_h = false
			elif Input.is_action_pressed("player_right"):
				if on_ground:
					walk_speed = -WALK_SPEED
					anim = "walking"
				else:
					walk_speed = -AIR_WALK_SPEED
				flip_h = true
			else:
				walk_speed *= WALK_DAMPER
		# Gravity is left
		if (sprite_rot2 > PI / 4 && sprite_rot2 <  3 * PI / 4):
			if Input.is_action_pressed("player_down"):
				if on_ground:
					walk_speed = WALK_SPEED
					anim = "walking"
				else:
					walk_speed = AIR_WALK_SPEED
				flip_h = false
			elif Input.is_action_pressed("player_up"):
				if on_ground:
					walk_speed = -WALK_SPEED
					anim = "walking"
				else:
					walk_speed = -AIR_WALK_SPEED
				flip_h = true
			else:
				walk_speed *= WALK_DAMPER
	set_player_animation(anim, flip_h)

	# Do actual movement
	var collision = move_and_collide(fall_velocity * simulation_speed, true, true, true)
	if collision != null and collision.get_collider().get("IS_ROCKET") != null:
		kill()
		collision.get_collider().explode()
	if collision != null and collision.collider.get("IS_MOVING_PLATFORM") != null:
		if not sticking and not sprite_rotating and intersections == 0:
			if fall_velocity.length() < SLOW_LAND:
				set_player_animation("landing_slow", flip_h)
			else:
				set_player_animation("landing", flip_h)
			sticking = true
			sticking_to = collision.collider
			if collision.get_collider().get("is_springy_platform"):
				collision.get_collider().position += fall_velocity
		gravity_charges_used = 0
		
	# ZS-276
	if sticking:
		fall_velocity = move_and_slide(fall_velocity * simulation_speed / delta) * delta / simulation_speed
		fall_velocity *= FALL_VELOCITY_DAMPER
		var col2 = move_and_collide(1000 * gravity * simulation_speed, true, true, true)
		if col2 != null and col2.collider == sticking_to:
			position += sticking_to.velocity * simulation_speed
			on_ground = true
		else:
			sticking = false
			on_ground = false
	else:
		var true_fall_velocity = move_and_slide(fall_velocity * simulation_speed / delta) * delta / simulation_speed
		var absorbed_velocity = fall_velocity - true_fall_velocity
		if absorbed_velocity.length() > ABSORBED_VELOCITY_THRESHOLD:
			true_fall_velocity *= FALL_VELOCITY_DAMPER
			# Did your feet just come in contact with the ground?
			if not on_ground and not sprite_rotating and (gravity.normalized() - absorbed_velocity.normalized()).length() < 0.1:
				if fall_velocity.length() < SLOW_LAND:
					set_player_animation("landing_slow", flip_h)
				else:
					set_player_animation("landing", flip_h)
				on_ground = true
				gravity_charges_used = 0
			# Die if you hit ground going too fast
			if absorbed_velocity.length() > FALL_DEATH_SPEED and no_fall_death == 0:
				kill()
		else:
			if sticking:
				on_ground = true
			else:
				on_ground = false
		fall_velocity = true_fall_velocity
	move_and_slide(walk_speed * right_vector * simulation_speed / delta)
	$Label.text = str(intersections) + " " + str(SaveState.coin_count) + " " + str(fall_velocity.length()) + " " + str(on_ground) + " " + str(gravity_charges_used)
			
	# Threshold fall velocity (ZS-108)
	if (fall_velocity.length() < SLIDE_THRESHOLD):
		fall_velocity = Vector2(0, 0)

func _on_AnimatedSprite_animation_finished():
	one_shot_anim = false
	pass # Replace with function body.

func _on_Area2D_body_entered(body):
	if body is TileMap or body.get("IS_MOVING_PLATFORM") != null and body != sticking_to:
		intersections = intersections + 1
	pass # Replace with function body.


func _on_Area2D_body_exited(body):
	if body is TileMap or body.get("IS_MOVING_PLATFORM") != null and body != sticking_to:
		intersections = intersections - 1
	pass # Replace with function body.
