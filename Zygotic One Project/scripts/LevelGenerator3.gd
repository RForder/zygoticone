extends Node2D

# Modify till your heart is content
const WIDTH = 8
const HEIGHT = 8
const ROOM_CNT = 16
const LEVEL_CNT = 1 # Leave this at 1 for now. It's broken.
const RNG_SEED = 256
const LEVEL_SCENE_PREFIX = "AutogenLevel_"
const DEBUG = false # Change me to false to use real rooms, true to use debug rooms

# Do not modify
enum WALLS {
	ZERO,
	ONE,
	TWO_ADJACENT,
	TWO_OPPOSITE,
	THREE,	
	FOUR
}
const NORTH = 1
const SOUTH = 2
const EAST = 4
const WEST = 8

func load_scenes(debug):
	var scenes = [ [], [], [], [], [], [] ]
	
	# Empty rooms for testing.
	if debug:
		scenes[WALLS.ZERO].push_front(load("res://scenes/rooms/Room_0_###.tscn"))
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/Room_1_###.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/Room_2A_###.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/Room_2P_###.tscn"))
		scenes[WALLS.THREE].push_front(load("res://scenes/rooms/Room_3_###.tscn"))
		scenes[WALLS.FOUR].push_front(load("res://scenes/rooms/Room_0_###.tscn"))
	else:	
		scenes[WALLS.ZERO].push_front(load("res://scenes/rooms/Room_0_###.tscn"))
		#1 wall rooms
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/1_002_1_N.tscn"))
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/1_005_1_N.tscn"))
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/1_007_1_N.tscn"))
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/1_012_1_N.tscn"))
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/1_000_2_N.tscn"))
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/1_001_2_N.tscn"))
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/1_004_2_K.tscn"))
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/1_006_2_N.tscn"))
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/1_008_2_N.tscn"))
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/1_009_2_N.tscn"))
		scenes[WALLS.ONE].push_front(load("res://scenes/rooms/1_010_2_N.tscn"))
		#2 adjacent wall rooms
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_000_1_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_004_1_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_006_1_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_008_1_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_024_1_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_003_2_K.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_011_2_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_014_2_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_015_2_K.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_016_2_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_021_2_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_023_2_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_025_2_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_027_2_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_030_2_N.tscn"))
		scenes[WALLS.TWO_ADJACENT].push_front(load("res://scenes/rooms/2A_031_2_N.tscn"))
				#2 parallel wall rooms
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_014_1_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_000_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_002_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_003_2_K.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_005_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_006_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_007_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_008_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_010_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_012_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_018_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_019_2_K.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_020_2_K.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_023_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_024_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_025_2_K.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_026_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_027_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_030_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_031_2_N.tscn"))
		scenes[WALLS.TWO_OPPOSITE].push_front(load("res://scenes/rooms/2P_032_2_N.tscn"))
		#3 wall rooms
		scenes[WALLS.THREE].push_front(load("res://scenes/rooms/3_003_2_K.tscn"))
		scenes[WALLS.THREE].push_front(load("res://scenes/rooms/3_004_2_N.tscn"))
		scenes[WALLS.THREE].push_front(load("res://scenes/rooms/3_005_2_K.tscn"))
		scenes[WALLS.THREE].push_front(load("res://scenes/rooms/3_006_2_K.tscn"))
		#4 wall rooms
		scenes[WALLS.FOUR].push_front(load("res://scenes/rooms/Room_4_000.tscn"))
	return scenes

# Called when the node enters the scene tree for the first time.
func _ready():
	var prng = RandomNumberGenerator.new()
	var i = 0
	prng.seed = RNG_SEED	
	var level = generate_level(prng)
	
	var level_node = generate_level_scene(level, load_scenes(true), prng, i)	
	generate_minimap(level_node, i)
	var mm = create_mm(i)
	mm.name = "Minimap"	

	level_node = generate_level_scene(level, load_scenes(false), prng, i)
	level_node.add_child(mm)
	mm.owner = level_node
	
	var ps = PackedScene.new()
	var result = ps.pack(level_node)
	ResourceSaver.save("res://scenes/levels/autogen/" + LEVEL_SCENE_PREFIX + str(i) + ".tscn", ps)
	return
	
func create_mm(i):
	var s = Sprite.new()
	s.texture = load("res://scenes/levels/autogen/" + LEVEL_SCENE_PREFIX + str(i) + ".png")
	assert(s.texture != null)
	return s

func generate_minimap(level_node, i):
	add_child(level_node)
	get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ALWAYS)
	get_viewport().size = Vector2(WIDTH * 64, HEIGHT * 64)
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,  SceneTree.STRETCH_ASPECT_IGNORE, Vector2(WIDTH * 64, HEIGHT * 64), 1)
	get_tree()
	$Camera2D.zoom = Vector2(32, 32)
	$Camera2D.position = Vector2(2048 * WIDTH / 2, 2048 * HEIGHT / 2)
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")	
	var img = get_viewport().get_texture().get_data()
	assert(img.save_png("res://scenes/levels/autogen/" + LEVEL_SCENE_PREFIX + str(i) + ".png") == 0)
	remove_child(level_node)
	return img
	
func get_room(scenes, walls, prng):
	var room_rot = 0
	var style = 0
	print(walls)
	if walls == 0:
		room_rot = 0
		style = WALLS.ZERO
	elif walls == NORTH:
		room_rot = 2
		style = WALLS.ONE
	elif walls == SOUTH:
		room_rot = 0
		style = WALLS.ONE
	elif walls == EAST:
		room_rot = 3
		style = WALLS.ONE
	elif walls == WEST:
		room_rot = 1
		style = WALLS.ONE
	elif walls == NORTH + SOUTH:
		room_rot = 0
		style = WALLS.TWO_OPPOSITE
	elif walls == NORTH + EAST:
		room_rot = 3
		style = WALLS.TWO_ADJACENT
	elif walls == NORTH + WEST:
		room_rot = 2
		style = WALLS.TWO_ADJACENT
	elif walls == SOUTH + EAST:
		room_rot = 0
		style = WALLS.TWO_ADJACENT
	elif walls == SOUTH + WEST:
		room_rot = 1
		style = WALLS.TWO_ADJACENT
	elif walls == EAST + WEST:
		room_rot = 1
		style = WALLS.TWO_OPPOSITE
	elif walls == NORTH + SOUTH + EAST:
		room_rot = 0
		style = WALLS.THREE
	elif walls == NORTH + SOUTH + WEST:
		room_rot = 2
		style = WALLS.THREE
	elif walls == NORTH + EAST + WEST:
		room_rot = 3
		style = WALLS.THREE
	elif walls == SOUTH + EAST + WEST:
		room_rot = 1
		style = WALLS.THREE
	else:
		room_rot = 0
		style = WALLS.FOUR
	return [room_rot, scenes[style][prng.randi_range(0, scenes[style].size() - 1)].instance()]
	
func generate_level_scene(level, scenes, prng, i):
	var root = Node2D.new()
	root.name = LEVEL_SCENE_PREFIX + str(i)
	for i in range(HEIGHT * WIDTH):
		var x = i % WIDTH
		var y =  floor(i / WIDTH)
		var room_data = get_room(scenes, level[x][y], prng)
		var rot = room_data[0]
		var room = room_data[1]
		room.position.x = x * 2048
		room.position.y = y * 2048
		room.rotation = rot * PI / 2
		if rot == 1:
			room.position.x += 2048
		if rot == 2:
			room.position.y += 2048
			room.position.x += 2048
		if rot == 3:
			room.position.y += 2048
		root.add_child(room)
		room.owner = root
	return root

func generate_level(prng):
	# Initialize the level with four walls in every room
	var level_rooms = []
	var visited_rooms = []
	for x in range(WIDTH):
		level_rooms.push_front([])
		visited_rooms.push_front([])
	for x in range(WIDTH):
		for y in range(HEIGHT):
			level_rooms[x].push_front(NORTH + SOUTH + EAST + WEST)
			visited_rooms[x].push_front(false)
		
	# Choose the starting room at random
	var init_room = Vector2(prng.randi_range(0, WIDTH - 1), prng.randi_range(0, HEIGHT - 1))

	var current_room = init_room
	var visited_room_cnt = 0
	var maze_stack = []
	var finished = false

	visited_rooms[current_room.x][current_room.y] = true
	visited_room_cnt = visited_room_cnt + 1
	while visited_room_cnt < ROOM_CNT:
		print(current_room)
		
		# Figure out which neighbors of the current room haven't been visited
		var unvisited_neighbors = []
		if current_room.y > 0 and not visited_rooms[current_room.x][current_room.y - 1]:
			unvisited_neighbors.push_front([Vector2(current_room.x, current_room.y - 1), NORTH, SOUTH])
		if current_room.y < HEIGHT - 1 and not visited_rooms[current_room.x][current_room.y + 1]:
			unvisited_neighbors.push_front([Vector2(current_room.x, current_room.y + 1), SOUTH, NORTH])
		if current_room.x > 0 and not visited_rooms[current_room.x - 1][current_room.y]:
			unvisited_neighbors.push_front([Vector2(current_room.x - 1, current_room.y), WEST, EAST])
		if current_room.x < WIDTH - 1 and not visited_rooms[current_room.x + 1][current_room.y]:
			unvisited_neighbors.push_front([Vector2(current_room.x + 1, current_room.y), EAST, WEST])
			
		# If the current room has unvisited neighbors
		if unvisited_neighbors.size() > 0:
			
			# Choose randomly one of the unvisited neighbors
			var chosen_neighbor = unvisited_neighbors[prng.randi_range(0, unvisited_neighbors.size() - 1)]
			
			# Push current room onto stack
			maze_stack.push_front(current_room)
			
			# Remove walls between current room and chosen neighbor
			level_rooms[current_room.x][current_room.y] -= chosen_neighbor[1]
			level_rooms[chosen_neighbor[0].x][chosen_neighbor[0].y] -= chosen_neighbor[2]
			
			# Make chosen room current room and mark it as visited
			current_room = chosen_neighbor[0]
			visited_rooms[current_room.x][current_room.y] = true
			visited_room_cnt = visited_room_cnt + 1
			
		# If the current room has no unvisited neighbors
		else:
			# Pop last room off stack
			if maze_stack.size() > 0:
				current_room = maze_stack.pop_front()
			else:
				finished = true
	return level_rooms
