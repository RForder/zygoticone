extends Area2D

# dummy var used to identify coins
var IS_A_COIN = 0;

# get singleton
onready var gl_SaveState = get_node("/root/SaveState");

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var collected = false

# clear self if I've already been collected
func checkCoinCollection():
	if gl_SaveState.checkCoin(name):
		get_parent().call_deferred("remove_child", self);
		# call_deferred("remove_child", self);
		# get_parent().remove_child(self); # some Godot nonsense

# Called when the node enters the scene tree for the first time.
func _ready():
	checkCoinCollection();
	$AnimatedSprite.play("bounce")
	rotation = -get_parent().rotation
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Coin_body_entered(body):
	if body.name == "Player" and not collected:
		collected = true
		$AnimatedSprite.play("invis")
		$SFX.play()
		
		# call collect on save state
		gl_SaveState.collectCoin(name);
	return
