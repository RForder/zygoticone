extends KinematicBody2D

const SPEED = 10
const IS_ROCKET = true
var velocity = Vector2(0, 0)
var done = false

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.play("action")
	pass # Replace with function body.

func explode():
	$CollisionShape2D.disabled = true
	$AnimatedSprite.play("trigger")
	get_node("Rocket_Explode_SFX").play()
	done = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	# global pause
	if gl_SceneLoader.global_pause:
		return;
	
	# behave normally
	if done:
		return
	var collision = move_and_collide(velocity, true, true, true)
	if collision != null:
		if collision.get_collider().name == "Player":
			collision.get_collider().kill()
		if collision.get_collider().get("IS_ROCKET") == null:
			explode()
	position += velocity
	pass
