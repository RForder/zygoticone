extends Node

# loop through all nodes
func countCoins(node, num_coins):
	# check if it's a coin
	if "IS_A_COIN" in node:
		num_coins += 1;
	
	# keep looking
	for child in node.get_children():
		num_coins = countCoins(child, num_coins);
	return num_coins;

# list of levels to check
var base_path = "res://scenes/levels/Crust/";
var levels = [];

# Called when the node enters the scene tree for the first time.
func _ready():
	# add in level paths
	levels.push_back("CoinTest.tscn");
	levels.push_back("Level1_10_123.tscn");
	levels.push_back("Level2_8x8_16_256.tscn");
	levels.push_back("Level3_8x8_16_256.tscn");
	levels.push_back("Level4_8x8_16_310.tscn");
	levels.push_back("Level5_Miniboss.tscn");
	
	# check number of coins in level
	for path in levels:
		path = base_path + path;
		var level = load(path).instance();
		var coins = countCoins(level, 0);
		print(path + " has " + str(coins) + " coins");
