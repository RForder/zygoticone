extends Node2D

export var ButtonOn = false

func _ready():
	if ButtonOn:
		$AnimatedSprite.play("On")
	else:
		$AnimatedSprite.play("Off")
	pass # Replace with function body.

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		if ButtonOn:
			return
		#	$AnimatedSprite.play("OnToOff")
		else:
			$AnimatedSprite.play("OffToOn")
			ButtonOn = not ButtonOn	
			$Button_Push_SFX.play()	
	return
