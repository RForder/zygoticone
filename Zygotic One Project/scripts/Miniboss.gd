extends Node2D

const CAMERA_ZOOM = 2
const CAMERA_SCROLL_SPEED = 100
var level = null
var camera = null
var min_x = 0

func _ready():
	level = load("res://scenes/levels/MinibossDemo.tscn").instance()
	level.name = "Level"
	var player = level.get_node("Player")
	camera = player.get_node("Camera2D")
	player.remove_child(camera)
	$WorldContainer/WorldViewport.add_child(camera)
	$WorldContainer/WorldViewport.add_child(level)
	var prng = RandomNumberGenerator.new()
	for n in level.get_children():
		var tm = n.get_node("TileMap")
		if tm != null and tm is TileMap:
			tm.scale = Vector2(1, 1)
			tm.cell_size = Vector2(64, 64)
			tm.tile_set = load("res://sprites/TileSets/CrustTiles.tres")
			for x in range(32):
				for y in range(32):
					var ix = tm.get_cell(x, y)
					var variations = []
					if ix == 12:
						variations = [ 12, 40 ]
					elif ix == 13:
						variations = [ 13, 39 ]
					elif ix == 15:
						variations = [ 15, 38 ]
					elif ix == 16:
						variations = [ 16, 37 ]
					elif ix == 17:
						variations = [ 17, 33 ]
					elif ix == 18:
						variations = [ 18, 32 ]
					elif ix == 21:
						variations = [ 21, 36 ]
					elif ix == 23:
						variations = [ 23, 34 ]
					elif ix == 24:
						variations = [ 24, 35 ]
					else:
						variations = [ ix ]
					tm.set_cell(x, y, variations[prng.randi() % variations.size()], tm.is_cell_x_flipped(x, y), tm.is_cell_y_flipped(x, y), tm.is_cell_transposed(x, y))
	pass

func _process(delta):
	var player = level.get_node("Player")
	assert(player != null)	
	var reversing = Input.is_action_pressed("Time_Reverse") and level.get_node("Player").time_reversal_left > 0
	if reversing:
		min_x += -delta * CAMERA_SCROLL_SPEED
	else:
		min_x += delta * CAMERA_SCROLL_SPEED
	camera.position.y = player.position.y
	camera.position.x = max(min_x, player.position.x)
	$WorldContainer.get_material().set_shader_param("reversing", reversing)
	$HUD/CoinLabel.text = str(player.coins)
	$HUD/KeyLabel.text = str(player.keys)
	$HUD/TimeReversalBar.set_size(Vector2(player.time_reversal_left * 1820 / player.MAX_TIME_REVERSAL, 40))	
	pass
