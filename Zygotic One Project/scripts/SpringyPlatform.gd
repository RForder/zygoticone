extends KinematicBody2D

#A constant of 2.3 moves 5 squares. 
export var SPRING_CONSTANT = 3.2
export var SPRING_DAMPENER = 0.1
const IS_MOVING_PLATFORM = true
const MASS = 1

var t = 0
var velocity = Vector2(0, 0)
var initial_position = Vector2(0, 0)
var is_springy_platform = true

func _ready():
	initial_position = position
	pass

func _physics_process(delta):
	var delta_pos = position - initial_position
	var force = -SPRING_CONSTANT * delta_pos
	var accel = MASS * force
	velocity += accel * delta
	position += velocity
	velocity *= SPRING_DAMPENER
	t += delta
	
	pass
