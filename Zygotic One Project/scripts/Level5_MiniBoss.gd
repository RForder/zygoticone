extends Node2D

# the original load function
func origLoad():
	# stop music
	$Music_Level5.stop();
	$GrabbyHandsSFX.stop();
	
	# stop hazard
	start_hazard = false;
	die_once = true;
	
	# check children at start
	# for child in $WorldContainer/WorldViewport.get_children():
		# print(child.name);
	
	# check ready time
	var start_time = OS.get_ticks_msec();
	
	# load level
	level = load("res://scenes/levels/Crust/Level5_Miniboss.tscn").instance();
	level.name = "Level"
	
	# get player references
	player = level.get_node("Player")
	camera = player.get_node("Camera2D")
	player.remove_child(camera)

	# check that the references got reloaded
	print("Player: " + player.name);
	print("Camera: " + camera.name);

	# attach camera
	$WorldContainer/WorldViewport.add_child(camera)
	$WorldContainer/WorldViewport.add_child(level)
	
#	# generate tiles
	var prng = RandomNumberGenerator.new()
	for n in level.get_children():
		var tm = n.get_node("TileMap")
		if tm != null and tm is TileMap:
			tm.scale = Vector2(1, 1)
			tm.cell_size = Vector2(64, 64)
			# tm.tile_set = load("res://sprites/TileSets/CrustTiles.tres")
			# tm.tile_set = load("res://sprites/TileSets/ProbeTiles.tres");
			tm.tile_set = load("res://sprites/TileSets/AltProbeTiles.tres");
			for x in range(32):
				for y in range(32):
					var fx = tm.is_cell_x_flipped(x, y)
					var fy = tm.is_cell_y_flipped(x, y)
					var ft = tm.is_cell_transposed(x, y)
					var ix = tm.get_cell(x, y)
					var variations = []
					if ix == 12:
						variations = [ 10, 11, 16]
#						var variations2 = [ 12, 40, 41 ]
#						if (tm.get_cell(x + 1, y) in variations2 and tm.get_cell(x - 1, y) in variations2 and tm.get_cell(x, y - 1) in variations2 and tm.get_cell(x, y + 1) in variations2):
#							variations = [ 41 ]
#						fx = false
#						fy = false
#						ft = false
					elif ix == 13: # buncha magic numbers # changed here from 13
						variations = [ 2 ] # [13, 39]
					elif ix == 15:
						variations = [ 3 ]
					elif ix == 16:
						variations = [ 4 ]
					elif ix == 17:
						variations = [ 22 ]
					elif ix == 18:
						variations = [ 8 ]
					elif ix == 21:
						variations = [ 5 ]
					elif ix == 23:
						variations = [ 27 ]
					elif ix == 24:
						variations = [ 9 ]
					elif ix == 30:
						variations = [0];
					else:
						variations = [ ix ]
					tm.set_cell(x, y, variations[prng.randi() % variations.size()], fx, fy, ft)
					
	# listen for signal from trigger switch
	level.get_node("TriggerSwitch").connect("switch_hit", self, "_on_TriggerSwitch_hit");
	
	# set initial "wall" position to match player
	min_x = camera.position.x; # - CAMERA_SCROLL_SPEED * 3; # 3 seconds till it reaches player
	min_x += 100;
	
	# pack up the scene
	# packed_scene.pack(level);
	# ResourceSaver.save("res://temp_scenes/PackedScene.tscn", packed_scene);
	
	# check ready time
	var end_time = OS.get_ticks_msec();
	print("Ready Milliseconds: " + str(end_time - start_time));

# "unloads" a level (resets WorldContainer)
func unload():
	# remove the current level instance
	# level.queue_free();
	
	# remove the added children
	# print("Children Before Remove: ");
	# for child in $WorldContainer/WorldViewport.get_children():
		# print(child.name);
	$WorldContainer/WorldViewport.remove_child(camera);
	$WorldContainer/WorldViewport.remove_child(level);
	camera.queue_free();
	level.queue_free();
	# print("Children After Remove: ");
	# for child in $WorldContainer/WorldViewport.get_children():
	# 	print(child.name);
	# print("**********************************************");

# "loads" a level (replaces a reference)
func loadLevel(next_level):
	# check children at start
	for child in $WorldContainer/WorldViewport.get_children():
		print(child.name);
	
	# check ready time
	var start_time = OS.get_ticks_msec();
	
	# load level
	level = next_level.instance();
	level.name = "Level"
	
	# get player references
	player = level.get_node("Player")
	camera = player.get_node("Camera2D")
	player.remove_child(camera)
	
	# check that the references got reloaded
	print("Player: " + player.name);
	print("Camera: " + camera.name);
	
	# attach camera
	$WorldContainer/WorldViewport.add_child(camera)
	$WorldContainer/WorldViewport.add_child(level)
	
	# listen for signal from trigger switch
	level.get_node("TriggerSwitch").connect("switch_hit", self, "_on_TriggerSwitch_hit");
	
	# set initial "wall" position to match player
	min_x = camera.position.x; # - CAMERA_SCROLL_SPEED * 3; # 3 seconds till it reaches player
	
	# check ready time
	var end_time = OS.get_ticks_msec();
	print("Ready Milliseconds: " + str(end_time - start_time));
	

const CAMERA_ZOOM = 2
const CAMERA_SCROLL_SPEED = 156 # how fast the wall moves 128
var start_hazard = false; # flipped once
var level = null
var min_x = 0

# hook to death wall
var death_wall = null;

# preload the scene for fast respawn
var packed_scene = PackedScene.new();
var level_scene = preload("res://temp_scenes/PackedScene.tscn");

# hook to player and camera
var player = null;
var camera = null;
var init_camera_pos = 0;
var die_once = true;

# public func for a trigger to call to start hazard
func triggerHazard():
	# once on, no one turns it off
	start_hazard = true;
	$WorldContainer/WorldViewport/GrabbyHands/AnimatedSprite.visible = true
	
# switch_hit signal reaction
func _on_TriggerSwitch_hit():
	triggerHazard();
	$Music_Level5.play();
	$GrabbyHandsSFX.play();
# get singleton
onready var gl_SaveState = get_node("/root/SaveState");

var tmr = Timer.new()

func _ready():
	# hide mouse cursor when inactive
	tmr.one_shot = true
	tmr.wait_time = 1.2
	tmr.connect("timeout", self, "hide_cursor")
	add_child(tmr)
	
	# set level in save state
	gl_SaveState.current_level = 5;
	# SceneLoader.last_loaded_scene = get_tree().current_scene.filename;
	# print(get_tree().current_scene.filename);
	# SceneLoader.final_dialogue_boxes = true;
	
	# hook to the death wall
	death_wall = $GrabbyHands;
	
	# attach death wall
	death_wall.get_parent().remove_child(death_wall);
	$WorldContainer/WorldViewport.add_child(death_wall);
	
	# load the level
	# loadLevel(level_scene);
	origLoad();

func _process(delta):
	# get the player
	assert(player != null)	
	
	# check for time reverse movement
	# "reversing" is a boolean flag used later
	var reversing = Input.is_action_pressed("Time_Reverse") and player.time_reversal_left > 0

	# autoscrolling
	if start_hazard:
		if not SceneLoader.dialogue_is_playing and not SceneLoader.global_pause:
			min_x += delta * CAMERA_SCROLL_SPEED

	# check for death
	if player.position.x < min_x and die_once:
		player.set_player_animation("death", false);
		player.dead = true;
		$Timer.wait_time = 0.6;
		$Timer.start();
		die_once = false;
		# unload();
		# loadLevel(level_scene);
		# origLoad();
		
		# don't replay dialogue
		SceneLoader.final_dialogue_boxes = false;	
	
	# make camera follow player
	camera.position.y = player.position.y;
	camera.position.x = max(player.position.x, min_x + (1920 / 2.0) + 100);
	# GODOT is doing something weird with its interpretation of viewport size
	
	# move death wall
	death_wall.position.x = min_x - 200; # + 355;
	death_wall.position.y = camera.get_camera_screen_center().y - 520; 
	
	# check positions
#	print("Camera Position: " + str(camera.position.x));
#	print("Min X: " + str(min_x));
#	print("Death Wall: " + str(death_wall.position.x));
#	print("Viewport X: " + str(get_viewport().size.x));
#	print("Viewport Y: " + str(get_viewport().size.y));
#	print("Width: " + str(OS.get_window_size().x));
#	print("Height: " + str(OS.get_window_size().y));
#
#	print("Project Width: " + str(ProjectSettings.get("display/window/size/width")));
	
	# apply time-reverse shader effect
	$WorldContainer.get_material().set_shader_param("reversing", reversing)
	$HUD/CoinLabel.text = str(SaveState.coin_count)
	$HUD/KeyLabel.text = str(player.keys)
	
	# calculate percentage for remaining player reversal
	# HUD stuff should be done with a singleton instead of this nonesense
	# $Bar.texture.set_region(Rect2(0, 0, img_width * percent ,125));
	var BAR_WIDTH = $HUD.bar_width;
	var BAR_HEIGHT = 125;
	var percent = float(player.time_reversal_left) / float(player.current_max_time_reversal);
	$HUD/ReverseBar.texture.set_region(Rect2(0, 0, BAR_WIDTH * percent, BAR_HEIGHT));
	# $HUD/TimeReversalBar.set_size(Vector2(player.time_reversal_left * 1820 / player.MAX_TIME_REVERSAL, 40))


func _on_Timer_timeout():
	print("TIMER TIMEDOUT");
	$WorldContainer/WorldViewport/GrabbyHands/AnimatedSprite.visible = false;
	$Timer.stop();
	unload();
	origLoad();

# hide mouse cursor when inactive
func _input(e: InputEvent):
	if e is InputEventMouseMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		tmr.start()
		
func hide_cursor():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
