extends AnimatedSprite

# blink randomly on timer
var rng = RandomNumberGenerator.new();
var timer = 0;
var min_time = 5.0; # seconds
var max_time = 15.0;
var elevator_on = false;

func _ready():
	rng.randomize();
	timer = rng.randf_range(min_time, max_time);


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# check timer
	timer -= delta;
	if timer <= 0 and not elevator_on:
		# reset timer and blink
		timer = rng.randf_range(min_time, max_time);
		frame = 0;
		play("blink");

# on animation
func _on_ActivationArea_body_entered(body):
	if body.name == "Player":
		play("TurnOn");
		elevator_on = true;

# off animation
func _on_ActivationArea_body_exited(body):
	if body.name == "Player":
		play("TurnOff");
		elevator_on = false;
