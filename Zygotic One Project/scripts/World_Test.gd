extends Node2D

const CAMERA_ZOOM = 2

var level = null
var mm = null

# Called when the node enters the scene tree for the first time.
func _ready():
	level = load("res://scenes/levels/autogen/World_Test.tscn").instance()
	mm = level.get_node("Minimap")
	level.remove_child(mm)
	$MinimapContainer/MinimapViewport.add_child(mm)
	mm.flip_v = true
	mm.position = Vector2.ZERO
	level.name = "Level"
	$WorldContainer/WorldViewport.add_child(level)
	$MinimapContainer/MinimapViewport/Camera2D.zoom = Vector2(CAMERA_ZOOM, CAMERA_ZOOM)
	var prng = RandomNumberGenerator.new()
	for n in level.get_children():
		var tm = n.get_node("TileMap")
		if tm != null and tm is TileMap:
			tm.scale = Vector2(1, 1)
			tm.cell_size = Vector2(64, 64)
			tm.tile_set = load("res://sprites/TileSets/CrustTiles.tres")
			for x in range(32):
				for y in range(32):
					var ix = tm.get_cell(x, y)
					var variations = []
					if ix == 12:
						variations = [ 12, 40 ]
					elif ix == 13:
						variations = [ 13, 39 ]
					elif ix == 15:
						variations = [ 15, 38 ]
					elif ix == 16:
						variations = [ 16, 37 ]
					elif ix == 17:
						variations = [ 17, 33 ]
					elif ix == 18:
						variations = [ 18, 32 ]
					elif ix == 21:
						variations = [ 21, 36 ]
					elif ix == 23:
						variations = [ 23, 34 ]
					elif ix == 24:
						variations = [ 24, 35 ]
					else:
						variations = [ ix ]
					tm.set_cell(x, y, variations[prng.randi() % variations.size()], tm.is_cell_x_flipped(x, y), tm.is_cell_y_flipped(x, y), tm.is_cell_transposed(x, y))
	update()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var player = level.get_node("Player")
	assert(player != null)
	$WorldContainer.get_material().set_shader_param("reversing", Input.is_action_pressed("Time_Reverse") and player.time_reversal_left > 0)
	$MinimapContainer.set_position(Vector2(get_viewport_rect().size.x - $MinimapContainer.get_rect().size.x / 2 - 16, 16))
	# If this fails, it means you didn't put the player in the level you specified
	var pos = ((level.get_node("Player").get_position() / 32) - (mm.texture.get_size() / 2))
	$MinimapContainer/MinimapViewport/Camera2D.position = pos
	$HUD/CoinLabel.text = str(player.coins)
	$HUD/KeyLabel.text = str(player.keys)
	$HUD/TimeReversalBar.set_size(Vector2(player.time_reversal_left * 1820 / player.MAX_TIME_REVERSAL, 40))
	pass
