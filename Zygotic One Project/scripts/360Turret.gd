extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var minimum_rotation = -179.9
export var maximum_rotation = 179.9
var target_rotation = 0
var pointed_at_player = false
var player = null

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");

func find_player_recursive(node):
	var p = null
	for child in node.get_children():
		if child.get_name() == "Player":
			return child
		else:
			p = find_player_recursive(child)
			if p != null:
				return p
	return null

func find_player():
	var root = get_tree().get_root();
	return find_player_recursive(root)
	
# Called when the node enters the scene tree for the first time.
func _ready():
	player = find_player()
	assert(player != null)
	pass # Replace with function body.


func _physics_process(_delta):
	# global pause
	if gl_SceneLoader.global_pause:
		return;
	
	# behave normally
	target_rotation = (position - player.position).angle() - rotation
	while target_rotation < 0:
		target_rotation += 2 * PI
	target_rotation = fmod(target_rotation, 2 * PI)
	var minr = minimum_rotation + 90
	while minr < 0:
		minr += 360
	minr = minr * 2 * PI / 360
	var maxr = maximum_rotation + 90
	while maxr < 0:
		maxr += 360
	maxr = maxr * 2 * PI / 360
	if fmod(maxr - minr + 2 * PI, 2 * PI) > fmod(target_rotation - minr + 2 * PI, 2 * PI):
		$Cannon.rotation = target_rotation
		$Label.text = str(target_rotation)
		pointed_at_player = true
	else:
		pointed_at_player = false
	pass

func _on_Timer_timeout():
	if not player.dead and pointed_at_player and not gl_SceneLoader.global_pause:
		var rocket = load("res://scenes/obstacles/Rocket.tscn").instance()
		add_child(rocket)
		rocket.owner = self	
		var dir = -Vector2(cos($Cannon.rotation), sin($Cannon.rotation))
		rocket.velocity = rocket.SPEED * dir
		rocket.position = $Cannon.position + 120 * dir
		$Turret_SFX.play()
	pass
