extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	$PressurePlateDoor3/DoorTriggerShape.position = $VerticalMovingPlatform.position
	$PressurePlateDoor4/DoorTriggerShape.position = $VerticalMovingPlatform2.position
	$PressurePlateDoor5/DoorTriggerShape.position = $LinearMovingPlatform.position
	pass
