extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_GravityChangeArea_body_entered(body):
	if body.name == "Player":
		var rot = fmod(rotation + 2 * PI, 2 * PI)
		body.gravity_rot = rot
		body.sprite_rotating = true
		if abs(rot - 0) < 0.1:
			body.last_grav = "down"
		if abs(rot - PI / 2) < 0.1:
			body.last_grav = "left"
		if abs(rot - PI) < 0.1:
			body.last_grav = "up"
		if abs(rot - 3 * PI / 2) < 0.1:
			body.last_grav = "right"
		body.set_player_animation("gravity_change", body.flip_h)
		body.get_node("Player_SFX/Gravity_Shift2").play()
		body.gravity_charges_used = 2
	pass # Replace with function body.
