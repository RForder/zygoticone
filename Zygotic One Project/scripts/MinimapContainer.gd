extends ViewportContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var marker = null
var border = null

# Called when the node enters the scene tree for the first time.
func _ready():
	marker = load("res://sprites/minimap/marker.png")
	border = load("res://sprites/minimap/border.png")
	pass # Replace with function body.

func _draw():
	draw_texture(marker, (get_size() / 4) - marker.get_size() / 2, Color(1, 1, 1, 1))
	draw_texture(border, Vector2.ZERO, Color(1, 1, 1, 1))


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
