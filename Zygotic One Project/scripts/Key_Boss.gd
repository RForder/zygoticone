extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var picked_up = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.play("Idle")
	rotation = -get_parent().rotation
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Key_body_entered(body):
	if body.name == "Player" and not picked_up:
		$AnimatedSprite.play("Blank")
		$Key_Boss_SFX.play()
		picked_up = true
		body.boss_keys = body.boss_keys + 1
	pass # Replace with function body.
