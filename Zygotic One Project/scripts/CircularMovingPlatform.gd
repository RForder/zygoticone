extends KinematicBody2D

export var Radius = 5 * 64 # Pixels
export var Frequency = 0.1 # Hertz
export var Phase = 0.00000 # Radians
export var Anti = false
var initial_position = Vector2(0, 0)
var player = null
const IS_MOVING_PLATFORM = true

var t = 0
var velocity = Vector2(0, 0)

# get singleton
onready var gl_SceneLoader = get_node("/root/SceneLoader");

func find_player_recursive(node):
	var p = null
	for child in node.get_children():
		if child.get_name() == "Player":
			return child
		else:
			p = find_player_recursive(child)
			if p != null:
				return p
	return null

func find_player():
	var root = get_tree().root.get_child(0)
	return find_player_recursive(root)

func _ready():
	initial_position = position
	player = find_player()
	if Anti:
		$Sprite.visible = false
		$Anti.visible = true
	else:
		$Sprite.visible = true
		$Anti.visible = false
	position.x = initial_position.x + Radius * cos(2 * PI * Frequency * t + Phase)
	position.y = initial_position.y + Radius * sin(2 * PI * Frequency * t + Phase)
	return
	
func _physics_process(delta):
	# global pause
	if gl_SceneLoader.global_pause:
		velocity = Vector2(0, 0);
		return;
	
	# behave normally
	if Anti and not (Input.is_action_pressed("Time_Reverse") and player.time_reversal_left > 0):
		velocity = Vector2(0, 0)
		return	
	position.x = initial_position.x + Radius * cos(2 * PI * Frequency * t + Phase)
	position.y = initial_position.y + Radius * sin(2 * PI * Frequency * t + Phase)
	t += delta
	velocity.x = (initial_position.x + Radius * cos(2 * PI * Frequency * t + Phase)) - position.x
	velocity.y = (initial_position.y + Radius * sin(2 * PI * Frequency * t + Phase)) - position.y
	if get_parent() != null:
		velocity = velocity.rotated(get_parent().rotation)	
	pass
