extends Node2D
signal switch_hit

# same as the normal switch, but emits a signal

export var SwitchOn = false
var emit_once = true;

func _ready():
	if SwitchOn:
		$AnimatedSprite.play("On")
	else:
		$AnimatedSprite.play("Off")
	pass # Replace with function body.

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		# set animation
		SwitchOn = not SwitchOn
		$AnimatedSprite.play("Action", not SwitchOn)
		$Switch_SFX.play()
		
		# emit signal
		if emit_once:
			emit_once = false;
			emit_signal("switch_hit");
	return
