extends Area2D

export var SECONDS_UNTIL_BREAK = 1.5
export var Reversible = false

const MAX_TIME_REVERSE = 10 * 60
const TIME_TILL_HITBOX_DISAPPEARS = 1 * 60

var touched = false
var touching = false
var standing_time = 0
var t = 0
var break_time = -1
var broken = false

# Called when the node enters the scene tree for the first time.
func _ready():
	#rotation = -get_parent().rotation
	pass # Replace with function body.

func _physics_process(delta):
	if $Hitbox/AnimatedSprite.animation == "breaking" and $Hitbox/AnimatedSprite.frame == 8:
		$Hitbox/CollisionShape2D.disabled = broken

	if Input.is_action_pressed("Time_Reverse") and Reversible:
		t = t - delta
		if broken and t < break_time:
			broken = false
			touched = false
			standing_time = 0
			$Hitbox/AnimatedSprite.play("breaking", not broken)
	else:		
		t = t + delta
		if touching:
			touched = true
		if touched:
			standing_time += delta
		if standing_time > SECONDS_UNTIL_BREAK and not broken:
			break_time = t
			broken = true
			$Hitbox/AnimatedSprite.play("breaking", not broken)
			$Breakable_Platform_SFX.play()
	return


func _on_BreakablePlatform_body_entered(body):
	if body.name == "Player":
		touching = true
	pass # Replace with function body.

func _on_AnimatedSprite_animation_finished():
	pass # Replace with function body.


func _on_BreakablePlatform_body_exited(body):
	if body.name == "Player":
		touching = false
	pass # Replace with function body.
