extends Area2D
export var Level_Music = "Music_Crust"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.play("closed")
	get_node(Level_Music).play()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Door_body_entered(body):
	if body.name == "Player" and body.boss_keys > 0:
		body.boss_keys = body.boss_keys - 1
		$AnimatedSprite.play("opening")
		get_node(Level_Music).stop()
		$Door_Boss_SFX.play()
		$Music_Puzzle.play()
	pass # Replace with function body.


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "opening":
		$AnimatedSprite.play("open")
		$DoorHitbox/DoorHitboxShape.disabled = true
	pass # Replace with function body.
