extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var open = false
export var OpenDelay = 0

var touched = false
var time_since_touched = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	if open:
		$AnimatedSprite.animation = "open"
		$DoorHitbox/DoorHitboxShape.disabled = true
	else:
		$AnimatedSprite.animation = "closed"
		$DoorHitbox/DoorHitboxShape.disabled = false
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	if touched:
		time_since_touched = time_since_touched + delta
	if time_since_touched > OpenDelay:
		touched = false
		time_since_touched = 0
		$AnimatedSprite.play("opening", not open)
		$Door_SFX.play()
	return
	
func _on_Door_body_entered(body):
	print("Detected Body: " + body.name);
	if body.name == "Player":
		time_since_touched = 0
		# only open
		# if not open:
		# 	touched = true
		# open = true;
		open = not open; # can close again
		touched = true
		print("_on_Door_body_entered: Detected Player")
	pass # Replace with function body.


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "opening":
		if not open:
			$AnimatedSprite.play("closed")
			$DoorHitbox/DoorHitboxShape.disabled = false
		else:
			$AnimatedSprite.play("open")
			$DoorHitbox/DoorHitboxShape.disabled = true
	pass # Replace with function body.
