extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var used = false

# Called when the node enters the scene tree for the first time.
func _ready():
	# rotation = get_tree().get_root().rotation;
	
	rotation = -get_parent().rotation
	# rotation = 0;
	$AnimatedSprite.play("Time Icon")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_TimeReversalCharge_body_entered(body):
	if body.name == "Player" and body.time_reversal_left < body.current_max_time_reversal and not used:
		used = true
		$AnimatedSprite.play("used")
		body.time_reversal_left = body.time_reversal_left + 60
		$SFX_time_recharge.play()
		if body.time_reversal_left > body.current_max_time_reversal:
			body.time_reversal_left = body.current_max_time_reversal
	pass # Replace with function body.
