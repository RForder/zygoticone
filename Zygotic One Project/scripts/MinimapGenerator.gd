extends Node2D

const LEVEL_FILE = "res://scenes/levels/Crust/Level1_10_123.tscn"
const OUTPUT_FILE = "res://scenes/levels/Crust/Level1_10_123.png"

const WIDTH = 16
const HEIGHT = 16

# Called when the node enters the scene tree for the first time.
func _ready():
	var level = load(LEVEL_FILE).instance()
	strip(level)
	generate_minimap(level)
	return
	
func strip(level):
	level.remove_child(level.get_node("Minimap"))
	for child in level.get_children():
		if child is Node2D and (child.get_node("AnimatedSprite") == null and child.get_node("Label") == null): # Is room.
			for room_node in child.get_children():
				if room_node is TileMap:
					for x in range(32):
						for y in range(32):
							if room_node.get_cell(x, y) != -1:
								room_node.set_cell(x, y, 12)
					room_node.tile_set = load("res://sprites/mock/Testing_tile_sets.tres")
				elif child is TileMap:
					for x in range(32):
						for y in range(32):
							if room_node.get_cell(x, y) != -1:
								room_node.set_cell(x, y, 12)
					room_node.tile_set = load("res://sprites/mock/Testing_tile_sets.tres")
				else:
					child.remove_child(room_node)
		else:
			level.remove_child(child)
			
func generate_minimap(level_node):
	add_child(level_node)
	level_node.owner = self
	get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ALWAYS)
	get_viewport().size = Vector2(WIDTH * 64, HEIGHT * 64)
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,  SceneTree.STRETCH_ASPECT_IGNORE, Vector2(WIDTH * 64, HEIGHT * 64), 1)
	$Camera2D.zoom = Vector2(32, 32)
	$Camera2D.position = Vector2(2048 * WIDTH / 2, 2048 * HEIGHT / 2)
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")	
	var img = get_viewport().get_texture().get_data()
	assert(img.save_png(OUTPUT_FILE) == 0)
	return img
	
