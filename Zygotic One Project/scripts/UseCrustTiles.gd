extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var level = load("res://scenes/levels/autogen/World_Test.tscn").instance()
	var final_level = Node2D.new()	
	for n in level.get_children():
		var room = n.duplicate()
		final_level.add_child(room)
		room.owner = final_level
		var tm = room.get_node("TileMap")
		if tm != null and tm is TileMap:
			room.remove_child(tm)
			tm = tm.duplicate()
			tm.scale = Vector2(1, 1)
			tm.cell_size = Vector2(64, 64)
			room.add_child(tm)
			tm.owner = room
			print(room.get_name())
			
	var ps = PackedScene.new()
	var result = ps.pack(final_level)
	ResourceSaver.save("res://scenes/levels/autogen/World_Test2.tscn", ps)
	print("Okay")
	add_child(final_level)
	final_level.owner = self
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
